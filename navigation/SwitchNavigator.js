import React from 'react';
import { View, Text } from 'react-native';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

// screens
import Welcome from '../screens/Welcome';
import Registration from '../screens/Registration';
import VerificationCode from '../screens/VerificationCode';
import Thanks from '../screens/Thanks';
import Pin_Enter from '../screens/Registration/Pin_Enter';
import Next_Regis from '../screens/Registration/Next_Regis';
import Business_Profile from '../screens/Registration/Business_Profile';
import Search_Brand from '../screens/Registration/Search_Brand';
import Form_Profile from '../screens/Registration/Form_Profile';
import Pin_Regis from '../screens/Registration/Pin_Regis';
import Service_Type from '../screens/Registration/Service_Type';
import Reservation from '../screens/Registration/Reservation';
import Reservation_Config from '../screens/Registration/Reservation_Config';
import Guest_Reminder from '../screens/Registration/Guest_Reminder';
import Scan_Qr from '../screens/Child/Scan_Qr.js';
import Reservation_Queue from '../screens/Child/Reservation_Queue.js';
import Reservation_Daftar from '../screens/Child/Reservation_Daftar.js';
import Setting_Profile from '../screens/Child/Setting_Profile.js';
import Service_Configuration from '../screens/Child/Service_Configuration.js';
import Tab_Reserve from '../screens/Child/Tab_Reserve.js';
import Tab_GuestRemind from '../screens/Child/Tab_GuestRemind.js';
import Report_Manage from '../screens/Child/Report_Manage.js';
import QR_Code from '../screens/Child/QR_Code.js';
import Footer from '../screens/Child/Footer.js';
import Setting from '../screens/Child/Setting.js';
import ActionSheet from '../screens/Child/ActionSheet.js';
import Grafik from '../screens/Child/Grafik.js';
import Login from '../screens/Login';

// screens

console.disableYellowBox = true;

const Auth = createStackNavigator({
  Welcome: {
    screen: Welcome,
    navigationOptions: () => ({
      headerShown: false,
    }),
  },
  Login: {
    screen: Login,
    navigationOptions: ({ navigation }) => ({
      title: `Login`,
      headerTintColor: `rgba(255, 255, 255, 1)`,
      headerTitleAlign: 'center',
      headerStyle: {
        backgroundColor: `rgba(0, 145, 95, 1)`,
      }
    })
  },
  Registration: {
    screen: Registration,
    navigationOptions: ({ navigation }) => ({
      title: `Registration`,
      headerTitleAlign:'center',
      headerTintColor: `rgba(255, 255, 255, 1)`,
      headerStyle: {
        backgroundColor: `rgba(0, 145, 95, 1)`
      }
    })
  },
})

const Regis = createStackNavigator({
  Registration: {
    screen: Registration,
    navigationOptions: ({ navigation }) => ({
      title: `Registration`,
      headerTitleAlign:'center',
      headerTintColor: `rgba(255, 255, 255, 1)`,
      headerStyle: {
        backgroundColor: `rgba(0, 145, 95, 1)`
      }
    })
  },
  Pin_Regis: {
    screen: Pin_Regis,
    navigationOptions: ({ navigation }) => ({
      title: `Registration`,
      headerTitleAlign:'center',
      headerTintColor: `rgba(255, 255, 255, 1)`,
      headerStyle: {
        backgroundColor: `rgba(0, 145, 95, 1)`
      }
    })
  },
  Business_Profile: {
    screen: Business_Profile,
    navigationOptions: ({ navigation }) => ({
      title: `Registration`,
      headerTitleAlign:'center',
      headerTintColor: `rgba(255, 255, 255, 1)`,
      headerStyle: {
        backgroundColor: `rgba(0, 145, 95, 1)`
      }
    })
  },
  Form_Profile: {
    screen: Form_Profile,
    navigationOptions: ({ navigation }) => ({
      title: `Registration`,
      headerTitleAlign:'center',
      headerTintColor: `rgba(255, 255, 255, 1)`,
      headerStyle: {
        backgroundColor: `rgba(0, 145, 95, 1)`
      }
    })
  },
  Next_Regis: {
    screen: Next_Regis,
    navigationOptions: ({ navigation }) => ({
      title: `Registration`,
      headerTitleAlign:'center',
      headerTintColor: `rgba(255, 255, 255, 1)`,
      headerStyle: {
        backgroundColor: `rgba(0, 145, 95, 1)`
      }
    })
  },
  Search_Brand: {
    screen: Search_Brand,
    navigationOptions: ({ navigation }) => ({
      title: `Registration`,
      headerTitleAlign:'center',
      headerTintColor: `rgba(255, 255, 255, 1)`,
      headerStyle: {
        backgroundColor: `rgba(0, 145, 95, 1)`
      }
    })
  },
  Pin_Enter: {
    screen: Pin_Enter,
    navigationOptions: ({ navigation }) => ({
      title: `Registration`,
      headerTitleAlign:'center',
      headerTintColor: `rgba(255, 255, 255, 1)`,
      headerStyle: {
        backgroundColor: `rgba(0, 145, 95, 1)`,
      }
    })
  },
  Service_Type: {
    screen: Service_Type,
    navigationOptions: ({ navigation }) => ({
      title: `Registration`,
      headerTitleAlign:'center',
      headerTintColor: `rgba(255, 255, 255, 1)`,
      headerStyle: {
        backgroundColor: `rgba(0, 145, 95, 1)`,
      }
    })
  },
  Reservation: {
    screen: Reservation,
    navigationOptions: ({ navigation }) => ({
      title: `Registration`,
      headerTitleAlign:'center',
      headerTintColor: `rgba(255, 255, 255, 1)`,
      headerStyle: {
        backgroundColor: `rgba(0, 145, 95, 1)`,
      }
    })
  },
  Reservation_Config: {
    screen: Reservation_Config,
    navigationOptions: ({ navigation }) => ({
      title: `Registration`,
      headerTitleAlign:'center',
      headerTintColor: `rgba(255, 255, 255, 1)`,
      headerStyle: {
        backgroundColor: `rgba(0, 145, 95, 1)`,
      }
    })
  },
  Guest_Reminder: {
    screen: Guest_Reminder,
    navigationOptions: ({ navigation }) => ({
      title: `Registration`,
      headerTitleAlign:'center',
      headerTintColor: `rgba(255, 255, 255, 1)`,
      headerStyle: {
        backgroundColor: `rgba(0, 145, 95, 1)`,
      }
    })
  }
})

const Settings = createStackNavigator({
  Setting: {
    screen: Setting,
    navigationOptions: ({ navigation }) => ({
      title: `Setting`,
      headerTitleAlign:'center',
      headerTintColor: `rgba(255, 255, 255, 1)`,
      headerStyle: {
        backgroundColor: `rgba(0, 145, 95, 1)`,
      }
    })
  },
  Setting_Profile: {
    screen: Setting_Profile,
    navigationOptions: ({ navigation }) => ({
      title: `Profile`,
      headerTitleAlign:'center',
      headerTintColor: `rgba(255, 255, 255, 1)`,
      headerStyle: {
        backgroundColor: `rgba(0, 145, 95, 1)`,
      }
    })
  },
  Service_Configuration: {
    screen: Service_Configuration,
    navigationOptions: ({ navigation }) => ({
      title: `Setting`,
      headerTitleAlign:'center',
      headerTintColor: `rgba(255, 255, 255, 1)`,
      headerStyle: {
        backgroundColor: `rgba(0, 145, 95, 1)`,
      }
    })
  },
  // Tab_Reserve: {
  //   screen: Tab_Reserve,
  //   navigationOptions: ({ navigation }) => ({
  //     title: `Setting`,
  //     headerTitleAlign:'center',
  //     headerTitleStyle:{
  //       flex:1,
  //       textAlign:'center',
  // 
  //     },
  //     headerTintColor: `rgba(255, 255, 255, 1)`,
  //     headerStyle: {
  //       color: `rgba(255, 255, 255, 1)`,
  //       backgroundColor: `rgba(0, 145, 95, 1)`,
  //     }
  //   })
  // },
  // Tab_GuestRemind: {
  //   screen: Tab_GuestRemind,
  //   navigationOptions: ({ navigation }) => ({
  //     title: `Setting`,
  //     headerTitleAlign:'center',
  //     headerTitleStyle:{
  //       flex:1,
  //       textAlign:'center',
  // 
  //     },
  //     headerTintColor: `rgba(255, 255, 255, 1)`,
  //     headerStyle: {
  //       color: `rgba(255, 255, 255, 1)`,
  //       backgroundColor: `rgba(0, 145, 95, 1)`,
  //     }
  //   })
  // },
  Report_Manage: {
    screen: Report_Manage,
    navigationOptions: ({ navigation }) => ({
      title: `Report Management`,
      headerTitleAlign:'center',
      headerTintColor: `rgba(255, 255, 255, 1)`,
      headerStyle: {
        backgroundColor: `rgba(0, 145, 95, 1)`,
      }
    })
  },
  QR_Code: {
    screen: QR_Code,
    navigationOptions: ({ navigation }) => ({
      title: `QR Code`,
      headerTitleAlign:'center',
      headerTintColor: `rgba(255, 255, 255, 1)`,
      headerStyle: {
        backgroundColor: `rgba(0, 145, 95, 1)`,
      }
    })
  },
})

const Home = createStackNavigator({
    Reservation_Daftar: {
      screen: Reservation_Daftar,
      navigationOptions: ({ navigation }) => ({
        title: `Reservation List`,
        headerTitleAlign:'center',
        headerTintColor: `rgba(255, 255, 255, 1)`,
        headerStyle: {
          backgroundColor: `rgba(0, 145, 95, 1)`,
        }
      })
    },
    Scan_Qr: {
      screen: Scan_Qr,
      navigationOptions: ({ navigation }) => ({
        title: `Scan QR`,
        headerTitleAlign:'center',
        headerTintColor: `rgba(255, 255, 255, 1)`,
        headerStyle: {
          backgroundColor: `rgba(0, 145, 95, 1)`,
        }
      })
    },
    Reservation_Queue: {
      screen: Reservation_Queue,
      navigationOptions: ({ navigation }) => ({
        title: `Reservation Queue`,
        headerTitleAlign:'center',
        headerTintColor: `rgba(255, 255, 255, 1)`,
        headerStyle: {
          backgroundColor: `rgba(0, 145, 95, 1)`,
        }
      })
    },
    // Grafik: {
    //   screen: Grafik,
    //   navigationOptions: ({ navigation }) => ({
    //     title: `Grafik`,
    //     headerTitleAlign:'center',
    //     headerTitleStyle:{
    //       flex:1,
    //       textAlign:'center',
    // 
    //     },
    //     headerTintColor: `rgba(255, 255, 255, 1)`,
    //     headerStyle: {
    //       color: `rgba(255, 255, 255, 1)`,
    //       backgroundColor: `rgba(0, 145, 95, 1)`,
    //     }
    //   })
    // },
  },
);

const AppNavigator =  createSwitchNavigator({
    routeAuth : Auth,
    routeRegis : Regis,
    routeSettings : Settings,
    routeHome : Home,
  },
  {
    initialRouteName : 'routeAuth',
  }
)

export default createAppContainer(AppNavigator);