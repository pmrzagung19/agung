import auth from '@react-native-firebase/auth';
import {
    LOGIN_USER_SUCCESS,
    LOGIN_USER_FAILED,
    LOGIN_USER,
    REGISTER_USER_SUCCESS,
    REGISTER_USER_FAILED,
    REGISTER_USER,
    LOGOUT_USER,
    LOGOUT_USER_SUCCESS,
    LOGOUT_USER_FAILED
} from './types';

export const loginUserSuccess = (dispatch, user, email) => {
    dispatch( {
        type: LOGIN_USER_SUCCESS,
        payload: user,
        muncul: email
    });
};

export const loginUserFailed = (dispatch, email) => {
    dispatch({ 
        type: LOGIN_USER_FAILED ,
        muncul: email
    });
};

export const logoutUserSuccess = (dispatch, user) => {
    dispatch( {
        type: LOGOUT_USER_SUCCESS,
        payload: user
    });
};

export const logoutUserFailed = (dispatch) => {
    dispatch({ 
        type: LOGOUT_USER_FAILED 
    });
};

export const registerUserSuccess = (dispatch, user) => {
    dispatch( {
        type: REGISTER_USER_SUCCESS,
        payload: user
    });
};

export const registerUserFailed = (dispatch) => {
    dispatch({ 
        type: REGISTER_USER_FAILED 
    });
};

export const loginUser = ( email, password ) => {
    return (dispatch) => {
        dispatch({ type: LOGIN_USER });

        auth().signInWithEmailAndPassword( email, password )
            .then(user => loginUserSuccess( dispatch, user, email ))
            .catch(() => loginUserFailed( dispatch, email ))
    };
};

export const regisUser = ( email, password ) => {
    return (dispatch) => {
        dispatch({ type: REGISTER_USER});

        auth().createUserWithEmailAndPassword( email, password )
            .then((user => registerUserSuccess( dispatch, user )))
            .catch(() => registerUserFailed( dispatch ))
    }
}

export const logoutUser = () => {
    return (dispatch) => {
        dispatch({ type : LOGOUT_USER });

        auth().signOut()
            .then(user => loginUserSuccess( dispatch, user ))
            .catch(() => logoutUserFailed( dispatch ))
    }
}