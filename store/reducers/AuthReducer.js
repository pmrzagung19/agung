import {
    LOGIN_USER_SUCCESS,
    LOGIN_USER_FAILED,
    LOGIN_USER,
    REGISTER_USER_FAILED,
    REGISTER_USER_SUCCESS,
    REGISTER_USER,
    LOGOUT_USER,
    LOGOUT_USER_SUCCESS,
    LOGOUT_USER_FAILED
} from '../actions/types';

const INITIAL_STATE = {
    user: null,
    err_login: '',
    err_regis: '',
    err_logout: '',
    email: null,
    success: false,
    failed: false,
    loading: false
};

export default (state = INITIAL_STATE, action) => {
    console.log(action.payload);
    switch (action.type) {
        case LOGIN_USER_SUCCESS:
            return { ...state, user: action.payload, err_login: '', email: action.muncul,  loading: false, success: true, failed: false};
        case LOGIN_USER_FAILED:
            return { ...state, err_login: 'Authentication Failed  ', email: action.muncul,  loading: false, success: false, failed: true };
        case LOGIN_USER:
            return { ...state, loading: true, err_login: '', success: false, failed: false };
        case REGISTER_USER_SUCCESS:
            return { ...state, user: action.payload, err_regis: '', loading: false, success: true, failed: false };
        case REGISTER_USER_FAILED:
            return { ...state, err_regis: 'Registration Failed.', muncul: email,  loading: false, success: false, failed: true };
        case REGISTER_USER:
            return { ...state, loading: true, err_regis: '', success: false, failed: false};
        case LOGOUT_USER:
            return { ...state, loading: true, err_logout: ''}
        case LOGOUT_USER_SUCCESS:
            return { ...state, user: action.payload, loading: false, err_logout: ''}
        case LOGOUT_USER_FAILED:
            return { ...state, err_logout: 'Logout Failed', loading: false}
        default:
            return state;
    }
};