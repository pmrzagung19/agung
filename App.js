import React from 'react'
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import ReduxThunk from 'redux-thunk'
import SwitchNavigator from './navigation/SwitchNavigator'
import reducer from './store/reducers'

const App = () => {
	const store = createStore(reducer, {}, applyMiddleware(ReduxThunk));

	return (
		<Provider store={store}>
			<SwitchNavigator />
		</Provider>
	)
}

export default App;