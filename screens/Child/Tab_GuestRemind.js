import React, { Fragment } from "react";
import {
  Text,
  TextInput,
  View,
  Picker,
  TouchableHighlight,
  ScrollView,
  StyleSheet
} from "react-native";
import { Container, Content, Input, Form, Item, Label, Icon, Button, Row, Col } from "native-base";
import InputSpinner from "react-native-input-spinner";
import { Switch } from 'react-native-paper';
import { withNavigation } from 'react-navigation';

const styles = StyleSheet.create({
  spinner: {
    width: "auto",
    minWidth:300,
    borderBottomColor:'green',
    borderBottomWidth:1
	},
  sfca3b6fb: {
    fontWeight: `500`,
    fontSize: 25,
    marginBottom: 10
  },
  s1054bc31: {
    marginBottom: 10,
    color: `rgba(0, 0, 0, 1)`
  },
  sf16512cb: {
    fontSize: 20,
    fontWeight: `400`,
    borderBottomWidth: 3,
    borderBottomColor: `rgba(0, 145, 95, 1)`,
    textAlign: `left`
  },
  s0905a6f3: {
    justifyContent: `flex-start`,
    marginBottom: 10
  },
  s18cbd0b5: {
    marginBottom: 10,
    color: `rgba(0, 0, 0, 1)`
  },
  s7d8d8586: {
    fontSize: 20,
    fontWeight: `400`,
    borderBottomWidth: 3,
    borderBottomColor: `rgba(0, 145, 95, 1)`,
    textAlign: `left`
  },
  se97c925a: {
    justifyContent: `flex-start`,
    marginBottom: 10
  },
  s84f75f76: {
    marginBottom: 10,
    color: `rgba(0, 0, 0, 1)`
  },
  s9384d1a1: {
    fontSize: 20,
    fontWeight: `400`,
    borderBottomWidth: 3,
    borderBottomColor: `rgba(0, 145, 95, 1)`,
    textAlign: `left`
  },
  s18772e5f: {
    justifyContent: `flex-start`,
    marginBottom: 10
  },
  se66991ab: {
    marginBottom: 10,
    color: `rgba(0, 0, 0, 1)`
  },
  s36b417b0: {
    fontSize: 20,
    fontWeight: `400`,
    borderBottomWidth: 3,
    borderBottomColor: `rgba(0, 145, 95, 1)`,
    textAlign: `left`
  },
  sf734a2f7: {
    justifyContent: `flex-start`,
    marginBottom: 10
  },
  see86fb68: {
    marginBottom: 10,
    color: `rgba(0, 0, 0, 1)`
  },
  s5b0ad781: {
    justifyContent: `flex-start`,
    marginBottom: 10
  },
  s301174cb: {
    marginBottom: 10,
    color: `rgba(0, 0, 0, 1)`
  },
  sb70b07ca: {
    fontSize: 20,
    fontWeight: `400`,
    borderBottomWidth: 3,
    borderBottomColor: `rgba(0, 145, 95, 1)`,
    textAlign: `left`
  },
  sfb3e5edf: {
    justifyContent: `flex-start`,
    marginBottom: 50
  },
  s56e8ed08: {
    textAlign: `center`,
    fontSize: 20,
    color: `rgba(255, 255, 255, 1)`,
    fontWeight: `400`
  },
  s7fd2bed2: {
    backgroundColor: `rgba(0, 145, 95, 1)`,
    justifyContent: `center`,
    borderRadius: 75,
    height: 55
  },
  scd1d1d72: {
    flex: 1,
    alignItems: `stretch`,
    flexDirection: `column`,
    justifyContent: `flex-end`,
    marginTop: 125
  },
  s9eedc2ea: {
    flex: 2,
    alignItems: `stretch`,
    flexDirection: `column`,
    padding: 20,
    justifyContent: `flex-start`
  },
  sefd4c879: {
    flex: 1,
    alignItems: `stretch`,
    flexDirection: `column`
  }
});

class Guest_Reminder extends React.Component{
    constructor(props) {
        console.log('value')
        super(props)
        this.state = {
          value: 0,
          value1: 0,
          value2: 0,
          amount:0
        }
        this.amount = 0
      }
      changeAmount(text) {
        this.amount = text
      }

      state = {
        isSwitchOn: false,
      };
  
  render(){
    const { isSwitchOn } = this.state;
    const{navigate}=this.props.navigation;
    return (
      <Container>
          <Content contentContainerStyle={{flex:1,paddingHorizontal:20,paddingVertical:20}}>
            <Content contentContainerStyle={{backgroundColor:'',alignContent:'space-between'}}>
                <Text style={{color:'grey',fontSize:16,paddingLeft:13,marginBottom:5}}>Number of notification(s)</Text>               
                <Content contentContainerStyle={{flex:1,alignItems:'center'}}>
                  <InputSpinner
                    value={this.state.value}
                    style={styles.spinner}
                    rounded={false}
                    color={'#009371'}
                  />
                </Content>
                <Text style={{color:'grey',fontSize:16,paddingLeft:13,marginTop:20,marginBottom:5}}>Notification time interval (in second)</Text>
                <Content contentContainerStyle={{flex:1,alignItems:'center'}}>
                  <InputSpinner
                    value={this.state.value}
                    style={styles.spinner}
                    rounded={false}
                    color={'#009371'}
                  />
                </Content>
                <Content contentContainerStyle={{paddingVertical:10}}>
                    <Row style={{flex:1,backgroundColor:'',justifyContent:'space-between'}}>
                        <Text style={{color:'grey',fontSize:16,paddingLeft:10}}>Retake queue number</Text>
                        <Switch
                        value={isSwitchOn}
                        onValueChange={() =>
                        { this.setState({ isSwitchOn: !isSwitchOn }); }
                        }
                        />
                    </Row>
                </Content>
            </Content>
            <View style={{flex:0.25,backgroundColor:'',justifyContent:'flex-start'}}>
              <Button onPress={()=>navigate('routeHome')} style={{borderRadius:20,backgroundColor:'#009371',justifyContent:'center'}}>
                  <Text style={{fontSize:18,fontWeight:'bold',color:'white'}}>Save</Text>
              </Button>
            </View>
          </Content>
      </Container>
   );
  }
}

export default withNavigation(Guest_Reminder);

export { styles };