import React, { Fragment } from "react";
import {
  Text,
  TextInput,
  View,
  Picker,
  TouchableHighlight,
  ScrollView,
  StyleSheet
} from "react-native";
import { Container, Header, Content, Tab, Tabs } from 'native-base';
import GuestRemind from "./Tab_GuestRemind";
import Reserve from "./Tab_Reserve";

class Service_Configuration extends React.Component{
  
  render(){
    return (
        <Container>
        <Tabs tabBarUnderlineStyle={{backgroundColor:'#009371'}}>
          <Tab heading="Reservation" textStyle={{color:'#009371'}} activeTextStyle={{color:'#009371'}} tabStyle={{backgroundColor:'white'}} activeTabStyle={{backgroundColor:'white'}}>
            <Reserve />
          </Tab>
          <Tab heading="Guest Reminder" textStyle={{color:'#009371'}} activeTextStyle={{color:'#009371'}} tabStyle={{backgroundColor:'white'}} activeTabStyle={{backgroundColor:'white'}}>
            <GuestRemind/>
          </Tab>
        </Tabs>
      </Container>
   );
  }
}

export default Service_Configuration;
