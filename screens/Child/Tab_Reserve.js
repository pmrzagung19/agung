import React,{Component} from 'react';
import {
  StyleSheet
} from "react-native";
import { Container, Content, Row, Icon, Item, Label, View, Text, Button } from 'native-base';
import InputSpinner from 'react-native-input-spinner';
import { Switch } from 'react-native-paper';
import { withNavigation } from 'react-navigation';

class TabTwo extends Component {
  constructor(props) {
    console.log('value')
    super(props)
    this.state = {
      value: 0,
      value1: 0,
      value2: 0,
      amount:0
    }
    this.amount = 0
  }
  changeAmount(text) {
    this.amount = text
  }

  state = {
    isSwitchOn: false,
  };
  render(){
    const { isSwitchOn } = this.state;
    const{goBack}=this.props.navigation;
    return(
      <Container>
          <Content contentContainerStyle={{flex:1,paddingHorizontal:20,paddingVertical:20}}>
            <Content contentContainerStyle={{backgroundColor:'',alignContent:'space-between'}}>
                <Text style={{color:'grey',fontSize:16,paddingLeft:13}}>Max number of serving capacity (in person)</Text>
                <Content contentContainerStyle={{backgroundColor:'',height:60,paddingBottom:10,marginBottom:20}}>
                    <Row style={{}}>
                        <Row style={{justifyContent:'center',backgroundColor:''}}>
                            <Icon name='user' type='FontAwesome5' style={{marginTop:20,paddingRight:5,fontSize:23}}/>
                            <Item floatingLabel style={{width:60}}>
                                <Label>Senior</Label>
                            </Item>
                        </Row>
                        <Row style={{justifyContent:'center'}}>
                            <Icon name='user' type='FontAwesome5' style={{marginTop:20,paddingRight:5,fontSize:23}}/>
                            <Item floatingLabel style={{width:60}}>
                                <Label>Adult</Label>
                            </Item>
                        </Row>
                        <Row style={{justifyContent:'center'}}>
                            <Icon name='user' type='FontAwesome5' style={{marginTop:25,paddingRight:5,fontSize:18}}/>
                            <Item floatingLabel style={{width:60}}>
                                <Label>Kid</Label>
                            </Item>
                        </Row>
                    </Row>
                </Content>
                <Text style={{color:'grey',fontSize:16,paddingLeft:13,marginBottom:5}}>Max number of serving capacity (in person)</Text>               
                <Content contentContainerStyle={{flex:1,alignItems:'center'}}>
                  <InputSpinner
                    value={this.state.value}
                    style={styles.spinner}
                    rounded={false}
                    color={'#009371'}
                  />
                </Content>
                <Text style={{color:'grey',fontSize:16,paddingLeft:13,marginTop:20,marginBottom:5}}>Waiting time estimation (in minute)</Text>
                <Content contentContainerStyle={{flex:1,alignItems:'center'}}>
                  <InputSpinner
                    value={this.state.value}
                    style={styles.spinner}
                    rounded={false}
                    color={'#009371'}
                  />
                </Content>
                <Content contentContainerStyle={{paddingVertical:10}}>
                    <Row style={{flex:1,backgroundColor:'',justifyContent:'space-between'}}>
                        <Text style={{color:'grey',fontSize:16,paddingLeft:10}}>Anti-broker system</Text>
                        <Switch
                        value={isSwitchOn}
                        onValueChange={() =>
                        { this.setState({ isSwitchOn: !isSwitchOn }); }
                        }
                        />
                    </Row>
                </Content>
            </Content>
            <View style={{flex:0.25,backgroundColor:'',justifyContent:'flex-start'}}>
              <Button onPress={()=>goBack()} style={{borderRadius:20,backgroundColor:'#009371',justifyContent:'center'}}>
                  <Text style={{fontSize:18,fontWeight:'bold',color:'white'}}>Save</Text>
              </Button>
            </View>
          </Content>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  spinner: {
    width: "auto",
    minWidth:300,
    borderBottomColor:'green',
    borderBottomWidth:1
	},
  sfca3b6fb: {
    fontWeight: `500`,
    fontSize: 25,
    marginBottom: 10
  },
  s1054bc31: {
    marginBottom: 10,
    color: `rgba(0, 0, 0, 1)`
  },
  sf16512cb: {
    fontSize: 20,
    fontWeight: `400`,
    borderBottomWidth: 3,
    borderBottomColor: `rgba(0, 145, 95, 1)`,
    textAlign: `left`
  },
  s0905a6f3: {
    justifyContent: `flex-start`,
    marginBottom: 10
  },
  s18cbd0b5: {
    marginBottom: 10,
    color: `rgba(0, 0, 0, 1)`
  },
  s7d8d8586: {
    fontSize: 20,
    fontWeight: `400`,
    borderBottomWidth: 3,
    borderBottomColor: `rgba(0, 145, 95, 1)`,
    textAlign: `left`
  },
  se97c925a: {
    justifyContent: `flex-start`,
    marginBottom: 10
  },
  s84f75f76: {
    marginBottom: 10,
    color: `rgba(0, 0, 0, 1)`
  },
  s9384d1a1: {
    fontSize: 20,
    fontWeight: `400`,
    borderBottomWidth: 3,
    borderBottomColor: `rgba(0, 145, 95, 1)`,
    textAlign: `left`
  },
  s18772e5f: {
    justifyContent: `flex-start`,
    marginBottom: 10
  },
  se66991ab: {
    marginBottom: 10,
    color: `rgba(0, 0, 0, 1)`
  },
  s36b417b0: {
    fontSize: 20,
    fontWeight: `400`,
    borderBottomWidth: 3,
    borderBottomColor: `rgba(0, 145, 95, 1)`,
    textAlign: `left`
  },
  sf734a2f7: {
    justifyContent: `flex-start`,
    marginBottom: 10
  },
  see86fb68: {
    marginBottom: 10,
    color: `rgba(0, 0, 0, 1)`
  },
  s5b0ad781: {
    justifyContent: `flex-start`,
    marginBottom: 10
  },
  s301174cb: {
    marginBottom: 10,
    color: `rgba(0, 0, 0, 1)`
  },
  sb70b07ca: {
    fontSize: 20,
    fontWeight: `400`,
    borderBottomWidth: 3,
    borderBottomColor: `rgba(0, 145, 95, 1)`,
    textAlign: `left`
  },
  sfb3e5edf: {
    justifyContent: `flex-start`,
    marginBottom: 50
  },
  s56e8ed08: {
    textAlign: `center`,
    fontSize: 20,
    color: `rgba(255, 255, 255, 1)`,
    fontWeight: `400`
  },
  s7fd2bed2: {
    backgroundColor: `rgba(0, 145, 95, 1)`,
    justifyContent: `center`,
    borderRadius: 75,
    height: 55
  },
  scd1d1d72: {
    flex: 1,
    alignItems: `stretch`,
    flexDirection: `column`,
    justifyContent: `flex-end`,
    marginTop: 125
  },
  s9eedc2ea: {
    flex: 2,
    alignItems: `stretch`,
    flexDirection: `column`,
    padding: 20,
    justifyContent: `flex-start`
  },
  sefd4c879: {
    flex: 1,
    alignItems: `stretch`,
    flexDirection: `column`
  }
});

export default withNavigation(TabTwo);

export { styles };