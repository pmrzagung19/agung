import React, { Fragment } from "react";
import {
  Text,
  TextInput,
  View,
  TouchableHighlight,
  ScrollView,
  StyleSheet
} from "react-native";
import { SafeAreaView, StackActions, NavigationActions } from "react-navigation";
import { Container, Content, Input, Form, Item, Label, Icon, Button, Thumbnail, ListItem, Left, Right, Radio,Picker } from "native-base";
import Footer from "./Footer";
import { connect } from 'react-redux';
import {
  logoutUser
} from '../../store/actions';
import auth from '@react-native-firebase/auth';


const styles = StyleSheet.create({
  sfca3b6fb: {
    fontWeight: `500`,
    fontSize: 25,
    marginBottom: 10
  },
  s1054bc31: {
    marginBottom: 10,
    color: `rgba(0, 0, 0, 1)`
  },
  sf16512cb: {
    fontSize: 20,
    fontWeight: `400`,
    borderBottomWidth: 3,
    borderBottomColor: `rgba(0, 145, 95, 1)`,
    textAlign: `left`
  },
  s0905a6f3: {
    justifyContent: `flex-start`,
    marginBottom: 10
  },
  s18cbd0b5: {
    marginBottom: 10,
    color: `rgba(0, 0, 0, 1)`
  },
  s7d8d8586: {
    fontSize: 20,
    fontWeight: `400`,
    borderBottomWidth: 3,
    borderBottomColor: `rgba(0, 145, 95, 1)`,
    textAlign: `left`
  },
  se97c925a: {
    justifyContent: `flex-start`,
    marginBottom: 10
  },
  s84f75f76: {
    marginBottom: 10,
    color: `rgba(0, 0, 0, 1)`
  },
  s9384d1a1: {
    fontSize: 20,
    fontWeight: `400`,
    borderBottomWidth: 3,
    borderBottomColor: `rgba(0, 145, 95, 1)`,
    textAlign: `left`
  },
  s18772e5f: {
    justifyContent: `flex-start`,
    marginBottom: 10
  },
  se66991ab: {
    marginBottom: 10,
    color: `rgba(0, 0, 0, 1)`
  },
  s36b417b0: {
    fontSize: 20,
    fontWeight: `400`,
    borderBottomWidth: 3,
    borderBottomColor: `rgba(0, 145, 95, 1)`,
    textAlign: `left`
  },
  sf734a2f7: {
    justifyContent: `flex-start`,
    marginBottom: 10
  },
  see86fb68: {
    marginBottom: 10,
    color: `rgba(0, 0, 0, 1)`
  },
  s5b0ad781: {
    justifyContent: `flex-start`,
    marginBottom: 10
  },
  s301174cb: {
    marginBottom: 10,
    color: `rgba(0, 0, 0, 1)`
  },
  sb70b07ca: {
    fontSize: 20,
    fontWeight: `400`,
    borderBottomWidth: 3,
    borderBottomColor: `rgba(0, 145, 95, 1)`,
    textAlign: `left`
  },
  sfb3e5edf: {
    justifyContent: `flex-start`,
    marginBottom: 50
  },
  s56e8ed08: {
    textAlign: `center`,
    fontSize: 20,
    color: `rgba(255, 255, 255, 1)`,
    fontWeight: `400`
  },
  s7fd2bed2: {
    backgroundColor: `rgba(0, 145, 95, 1)`,
    justifyContent: `center`,
    borderRadius: 75,
    height: 55
  },
  scd1d1d72: {
    flex: 1,
    alignItems: `stretch`,
    flexDirection: `column`,
    justifyContent: `flex-end`,
    marginTop: 125
  },
  s9eedc2ea: {
    flex: 2,
    alignItems: `stretch`,
    flexDirection: `column`,
    padding: 20,
    justifyContent: `flex-start`
  },
  sefd4c879: {
    flex: 1,
    alignItems: `stretch`,
    flexDirection: `column`
  }
});

class Setting extends React.Component{

  handleLogout(){
    this.props.logoutUser();
  }

  componentDidMount(){
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'Welcome' })],
    });

    const { navigate, dispatch } = this.props.navigation;

    auth().onAuthStateChanged(function(user) {
      if (!user) {
        console.log('User has Logout')
        navigate('routeAuth')
      }
    });  
  }

  render(){
    const{icon} = this.props;
    const{navigate} = this.props.navigation;
    return (
        <Container style={{flex:1}}>
            <View style={{flex:0.2,marginBottom:25,padding:20}}>
                <Text style={{marginBottom:10,color:'grey'}}>Profile</Text>
                <View style={{flex:1,flexDirection:'row',justifyContent:'space-between',marginRight:15,alignItems:'center'}}>
                    <Text style={{fontSize:20}}>Sushi Tei Plaza Indonesia <Text>(021)278278278</Text></Text>
                    <Text onPress={()=>navigate('Setting_Profile')} style={{color:'#009371'}}>Edit</Text>
                </View>
            </View>
            <View style={{flex:1,padding:20}}>
                <Text style={{marginBottom:10,color:'grey'}}>Settings</Text>
                <Text onPress={()=>navigate('routeRegis')} style={{fontSize:20,borderBottomWidth:0.5,borderBottomColor:'grey',paddingBottom:13,paddingTop:13}}>Service configuration</Text>
                <Text onPress={()=>navigate('Report_Manage')} style={{fontSize:20,borderBottomWidth:0.5,borderBottomColor:'grey',paddingBottom:13,paddingTop:13}}>Report management</Text>
                <Text onPress={()=>navigate('QR_Code')} style={{fontSize:20,borderBottomWidth:0.5,borderBottomColor:'grey',paddingBottom:13,paddingTop:13}}>QR code</Text>
                <Text onPress={this.handleLogout.bind(this)} style={{fontSize:20,borderBottomWidth:0.5,borderBottomColor:'grey',paddingBottom:13,paddingTop:13}}>Logout</Text>
            </View>
            <Footer/>
        </Container>
   );
  }
}

const mapStateToProps = ({ auth }) => {
  const { err_logout, loading, success, failed } = auth;

  return { err_logout, loading, success, failed };
};

export default connect(mapStateToProps, { 
  logoutUser 
}) (Setting);