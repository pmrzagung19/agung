import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, Image, FlatList } from 'react-native';
import { Container, Header, Left, Right, Content, Footer, FooterTab, Card, CardItem, Picker, Body, Text, Button, Icon, Title, Subtitle, Thumbnail } from 'native-base';
import { BorderlessButton } from 'react-native-gesture-handler';
import { Col, Row, Grid } from 'react-native-easy-grid';

const dataArray1 = [
    { id: '8 person(s)', title: 'Billy Ellish Stanford' }
    { id1: '9 kali', title1: 'Jokowi' }

];
const dataArray2 = [
    { id1: '9 kali', title1: 'Jokowi' }

];


export default class CardExample extends Component {

    render() {
        return (
            <FlatList
                data={dataArray1}
                renderItem={this.rendercard}

            />
        )
    }

    rendercard = ({ item }) => {
        return (
            <Container>

                <Content contentContainerStyle={{ marginHorizontal: 5, marginTop: 5, marginBottom: 5 }}>
                    <Card >
                        <CardItem onPress={() => navigate('MyEvent')}>
                            <Left>
                                <Thumbnail
                                    source={require('../../assets/avatar.png')}
                                    style={{ width: 80, height: 80, borderRadius: 80, marginRight: 5 }} />
                                <View style={{ alignItems: 'flex-start', top: -10 }}>
                                    <Title style={{ color: 'black' }}>{item.id}</Title>
                                    <Subtitle style={{ color: 'grey' }}>{item.title}</Subtitle>
                                </View>
                            </Left>
                            <Right>
                                <View style={{ alignItems: 'flex-end' }}>
                                    <Picker style={{ position: "absolute", top: 0, right: 10, width: 90, height: 1000, justifyContent: 'center' }} mode='dropdown'>
                                        <Picker.Item label="Detail" value="Detail" />
                                    </Picker>
                                    <Icon style={{ alignItems: 'flex-end', color: 'black' }} name='dots-vertical' type='MaterialCommunityIcons' />
                                </View>
                            </Right>
                        </CardItem>
                    </Card>




                </Content>

            </Container>
        );
    }
}

const styles = StyleSheet.create({
});

