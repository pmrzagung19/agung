import React, { Component } from "react";
import {StyleSheet,View} from "react-native";
import { Container, Content, Form, Item, Label, Text, Row,Icon } from "native-base";
import Modal from "react-native-modal";
import InputSpinner from "react-native-input-spinner";
import { withNavigation } from 'react-navigation';


const styles = StyleSheet.create({

  spinner: {
    width: "100%",
    paddingHorizontal:10
  },
  

});


const _21 = props => {}
class TabInteraction extends Component{
  state = {
    isModalVisible1: false
  };

  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  };

  toggleModal1 = () => {
    this.setState({ isModalVisible1: !this.state.isModalVisible1 });
  };
  render(){
    const{navigate}=this.props.navigation
  return (
      <View style={{ flexDirection: 'row', backgroundColor: 'white'}}>
        <View style={{flex: 2, height: 60, backgroundColor: 'white'}}>
            <View style={{flex: 1,backgroundColor: 'white', borderColor: '#009372', borderTopRightRadius: 15, borderTopWidth: 2, borderRightWidth: 2}}/>
            <View style={{}}>
            <Icon onPress={()=>navigate('Reservation_Daftar')} name='menu' type='Entypo' style={{position:'absolute',fontSize:33,width:'20%',bottom:-15,alignSelf:'center',alignItems:'center',color:'#009372'}}/>
            </View>
            <View style={{flex: 1,backgroundColor: 'transparent'}}/>
        </View>
        <View style={{flex: 1, height: 60, backgroundColor: 'white'}}>
            <View  style={{flex: 2, height: 70, backgroundColor: 'white', }}>
                <View style={{justifyContent: 'center', alignItems: 'center',position: 'absolute', padding: 5, alignSelf: 'center', backgroundColor: '#009372', width: 60, height: 60, borderRadius: 15, bottom: 10, borderWidth: 0,  zIndex: 3, marginBottom: -35}}>
                <Icon onPress={this.toggleModal1} name="ios-qr-scanner" type='Ionicons' style={{fontSize:50,color:"white"}}/>
                </View>
            </View>
            <View  style={{marginTop: 9, marginHorizontal: -2, borderColor: '#009372', flex: 2, height: 70, backgroundColor: 'transparent', borderBottomWidth: 2, borderLeftWidth: 2, borderRightWidth: 2, borderBottomLeftRadius: 15, borderBottomRightRadius: 15 }}></View>
            <View  style={{flex: 1, height: 70, backgroundColor: 'transparent' }}></View>
        </View>
        <View style={{flex: 2, height: 60, backgroundColor: 'transparent', borderTopLeftRadius: 50}}>
            <View style={{flex: 1,backgroundColor: 'transparent', borderTopLeftRadius: 15, borderColor: '#009372', borderTopWidth: 2, borderLeftWidth: 2}}/>
            <View style={{}}>
            <Icon onPress={()=>navigate('Setting')} name='settings' type='Feather' style={{position:'absolute',fontSize:33,width:'25%',bottom:-15,alignSelf:'center',alignItems:'center',color:'#009372'}}/>
            </View>
            <View style={{flex: 1,backgroundColor: 'transparent'}}/>
        </View>
        <Modal
      isVisible={this.state.isModalVisible1} 
      backdropOpacity={0.8}
      animationType={"slide"}
      style={{
      }}>
        <View style={{backgroundColor:'white',padding:20,borderRadius:20,justifyContent:'center'}}>
          <Text style={{fontSize:25}}>Input <Text style={{fontWeight:'bold',fontSize:25}}>walk-in customer</Text></Text>
          <Form>
            <Item floatingLabel style={{borderBottomWidth:3}}>
              <Label style={{fontSize:18}}>Name</Label>
            </Item>
            <Item floatingLabel style={{borderBottomWidth:3}}>
              <Label style={{fontSize:18}}>Type of service</Label>
            </Item>
          </Form>
              <Text style={{marginLeft:15,fontSize:20,marginVertical:20}}>Number of persons</Text>
              <InputSpinner
                value={this.state.value}
                style={styles.spinner}
                rounded={true}
                color={'#009371'}
              />
          <View style={{justifyContent:'space-around',flexDirection:'row',alignItems:'flex-end',width:'100%'}}>
              <Text onPress={this.toggleModal1} style={{marginTop:25,fontWeight:'bold',fontSize:20,color:'grey'}}>CANCEL</Text>
              <Text onPress={this.toggleModal1} style={{marginTop:25,fontWeight:'bold',fontSize:20,color:'#009371'}}>ADD</Text>
          </View>
        </View>
    </Modal>
  </View>
  
      
    
//     <View>
//       <View style={{ position: 'absolute', padding: 5, alignSelf: 'center', backgroundColor: '#fff', width: 90, height: 90, borderRadius: 50, bottom: 10, zIndex: 2, borderWidth: 3, borderColor: '#009372' }}>
//           <Button
//               icon={{
//                   name: 'md-qr-scanner',
//                   type: 'Ionicons',
//                   color: '#fff',
//                   style: { marginRight: 0 }
//               }}
//               onPress={() => {
//               this.props.navigation.navigate("05");
//             }}
//               buttonStyle={{ backgroundColor: '#009372', width: 75, height: 75, borderRadius: 50 }}
//               containerViewStyle={{ alignSelf: 'center' }}
//           />
//       </View>
//       <View style={{ position: 'absolute', backgroundColor: 'white', bottom: 0, zIndex: 1, width: '100%', height: 70, flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 15, paddingVertical: 10, borderTopWidth: 3, borderColor: '#009372' , paddingHorizontal: 30 }}>
//           <Icon
//               name='menu'
//               type='Entypo'
//               color='#009372'
//               onPress={() => {}} // Ex : openDrawer() in react-navigation

//           />
//           <View style={{ flexDirection: 'row', justifyContent: 'center'}}>
//               <Icon
//                   name='settings'
//                   type='SimpleLineIcons'
//                   color='#009372'
//                   containerStyle={{ marginHorizontal: 10 }}
//               />
//           </View>
//       </View>
//   </View>
  );
}
}
export default withNavigation(TabInteraction);