import React, { Fragment } from "react";
import {
  Text,
  View,
  TextInput,
  TouchableHighlight,
  StyleSheet,
  ImageBackground
} from "react-native";
import { Container, Content, Col, Button, Thumbnail,Icon, Row, Card, CardItem, Body, Accordion, Picker, Form, Item, Label } from "native-base";
import { withNavigation } from 'react-navigation';
import Modal from "react-native-modal";
import InputSpinner from "react-native-input-spinner";
import Footer from "./Footer";
import {RNSlidingButton, SlideDirection} from 'rn-sliding-button';

const dataArray1 = [
    { title: "8 person(s)", content: "Billy Ellish Stanford" }
  ];
const dataArray2 = [
    { title: "6 person(s)", content: "Lorem ipsum dolor sit amet" }
  ];
const dataArray3 = [
    { title: "2 person(s)", content: "Lorem ipsum dolor sit amet" }
  ];

class QueueList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      swiped: false,
      leftSwiped: false,
      rightSwiped: false,
      isModalVisible: true
    };
  }

  onSlideRight = () => {
    this.props.navigation.navigate('routeSettings')
};

  onLeftSlide() {
    var self = this;
    this.setState({swiped: true, leftSwiped: true}, () => {
      setTimeout(() => self.setState({swiped: false, leftSwiped: false}), 2500);
    });
  }

  onRightSlide() {
    var self = this;
    this.setState({swiped: true, rightSwiped: true}, () => {
      setTimeout(() => self.setState({swiped: false, rightSwiped: false}), 2500);
    });
  }

  onBothSlide() {
    var self = this;
    this.setState({swiped: true, bothSwiped: true}, () => {
      setTimeout(() => self.setState({swiped: false, bothSwiped: false}), 2500);
    });
  }

  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  };

  toggleModal1 = () => {
    this.setState({ isModalVisible1: !this.state.isModalVisible1 });
  };

  _renderHeader(item, expanded) {
    return (
      <View style={{
        flexDirection: "row",
        padding: 10,
        justifyContent: "space-between",
        alignItems: "center" ,
        backgroundColor: "white" }}>
        <Text style={{ fontWeight: "600",fontSize:16 }}>
          {" "}{item.title}
        </Text>
        {expanded
          ? <Icon style={{ fontSize: 18 }} name="arrow-up" type='SimpleLineIcons' />
          : <Icon style={{ fontSize: 18 }} name="arrow-down" type='SimpleLineIcons' />}
      </View>
    );
  }
  _renderContent(item) {
    return (
      <View style={{flex:1,flexDirection:'column',justifyContent:'space-between',borderTopColor:'grey',borderTopWidth:1,marginHorizontal:10,paddingVertical:10}}>
        <View style={{flex:1,flexDirection:'row'}}>
          <Text
            style={{
              fontStyle: "italic",
            }}
          >
            {item.content}
          </Text>
            <View style={{flex:1,justifyContent:'center',alignItems:'flex-end'}}>
              <Picker style={{position: "absolute", top: 0,right:10, width: 90,height:1000,justifyContent:'center'}} mode='dropdown'>
                <Picker.Item label="Call" value="Call"/>
                <Picker.Item label="Remove" value="Remove"/>
                <Picker.Item label="Transfer" value="Transfer" />
              </Picker>
              <Icon style={{alignItems:'flex-end',color:'black'}} name='dots-vertical' type='MaterialCommunityIcons'/>
            </View>
        </View>
        <View style={{flex:1,alignSelf:'center',marginVertical:10}}>
        {/* <RNSlidingButton
              style={{
                width:320,
                borderRadius:30,
                backgroundColor:'#009371'
              }}
              height={65}
              onSlidingSuccess={this.onSlideRight}
              slideDirection={SlideDirection.RIGHT}>
              <View style={{flex:1,flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                <View style={{height:45,width:45,backgroundColor:'white',marginRight:15,justifyContent:'center',alignItems:'center',borderRadius:30}}>
                  <Icon
                    name='ios-arrow-forward' type='Ionicons'
                    style={{color:'#009371'}}
                  />
                </View>
                <Text numberOfLines={1} style={{color:'white',fontSize:16}}>
                  Slide to call next customer
                </Text>
              </View>
            </RNSlidingButton> */}
        </View>
      </View>
    );
  }
  render() {
    return (
      <Container >
      <Content contentContainerStyle={{flex:1,marginHorizontal:20,marginTop:10,marginBottom:25}}>
          <Text style={{marginVertical:10}}>Queue List</Text>
          <Content contentContainerStyle={{}}>
            <Accordion
              style={{marginBottom:10,borderWidth:0.4}}
              dataArray={dataArray1}
              animation={true}
              expanded={true}
              renderHeader={this._renderHeader}
              renderContent={this._renderContent}
            />
            <Accordion
              style={{marginBottom:10,borderWidth:0.4}}
              dataArray={dataArray2}
              animation={true}
              expanded={true}
              renderHeader={this._renderHeader}
              renderContent={this._renderContent}
            />
            <Accordion
              style={{marginBottom:10,borderWidth:0.4}}
              dataArray={dataArray3}
              animation={true}
              expanded={true}
              renderHeader={this._renderHeader}
              renderContent={this._renderContent}
            />
          </Content>
      </Content>
    </Container>
    );
  }
}

export default withNavigation(QueueList);

const styles = StyleSheet.create({
    Accordion:{
        elevation: 10,
        marginBottom:20
    },
    col1: {
      alignItems:'center',
    },
    sf5bacfa4: {
      fontSize: 25,
      textAlign: `center`,
      fontWeight: `400`
    },
    sf6f58cc7: {
      flex: 1,
      flexDirection: `column`,
      justifyContent: `center`
    },
    s28bd15d1: {
      fontSize: 25,
      width: `90%`,
      fontWeight: `400`,
      borderBottomWidth: 3,
      borderBottomColor: `rgba(0, 145, 95, 1)`,
      paddingLeft: 10
    },
    s1157eaee: {
      flex: 3,
      justifyContent: `center`
    },
    s4bb7eeaf: {
      flexDirection: `row`,
      justifyContent: `center`,
      alignItems: `stretch`,
      marginBottom: 20
    },
    s44f53c6e: {
      fontSize: 11,
      fontWeight: `400`,
      color: `rgba(0, 145, 95, 1)`,
      marginBottom: 20
    },
    s29adf86c: {
      textAlign: `center`,
      fontSize: 20,
      color: `rgba(255, 255, 255, 1)`,
      fontWeight: `400`
    },
    scf3d97cc: {
      backgroundColor: `rgba(0, 145, 95, 1)`,
      justifyContent: `center`,
      borderRadius: 75,
      alignItems: `stretch`,
      height: 55,
      borderColor: 'white',
      borderWidth: 1
    },
    s59ba8be0: {
      justifyContent: `center`,
      alignItems: `stretch`,
      marginBottom: 2,
      marginTop: 4
    },
    s48aa0650: {
      flex: 4,
      alignItems: `stretch`,
      flexDirection: `column`,
      padding: 20
    },
    s48aa0651: {
        flex: 1,
        alignItems: `stretch`,
        flexDirection: `column`,
        padding: 20
      },
      spinner: {
        width: "100%",
        paddingHorizontal:10
      },
    s13f64db2: {
      flex: 1,
      alignItems: `stretch`,
      flexDirection: `column`,
      backgroundColor: `rgba(255, 255, 255, 1)`
    }
  });