import React, { Component } from 'react'
import {
    LineChart,
    BarChart,
    PieChart,
    ProgressChart,
    ContributionGraph,
    StackedBarChart
  } from "react-native-chart-kit";
import { Container } from 'native-base';
import { Dimensions } from "react-native";
const screenWidth = Dimensions.get("window").width;

  export default class Grafik extends Component{
      render(){
          return(
                <Container>
                    <StackedBarChart
                        data={{
                            labels: ["Test1", "Test2"],
                            legend: ["L1", "L2", "L3"],
                            data: [[60, 60, 60], [30, 30, 60]],
                            barColors: ["#dfe4ea", "#ced6e0", "#a4b0be"]
                        }}
                        width={Dimensions.get("window").width - 50} // from react-native
                        height={220}
                        yAxisLabel={"Rp"}
                        chartConfig={{
                        backgroundColor: "green",
                        backgroundGradientFrom: "green",
                        backgroundGradientTo: "green",
                        decimalPlaces: 2, // optional, defaults to 2dp
                        color: (opacity = 1) => `white`,
                        labelColor: (opacity = 1) => `white`,
                        style: {
                            borderRadius: 16
                        }
                        }}
                        style={{
                        marginVertical: 8,
                        borderRadius: 16
                        }}
                    />
                    <BarChart
                        data={{
                        labels: ["Januari", "Februari", "Maret", "April", "Mei", "Juni"],
                        datasets: [
                            {
                            data: [
                                Math.random() * 100,
                                Math.random() * 100,
                                Math.random() * 100,
                                Math.random() * 100,
                                Math.random() * 100,
                                Math.random() * 100
                            ]
                            }
                        ]
                        }}
                        width={Dimensions.get("window").width - 50} // from react-native
                        height={220}
                        yAxisLabel={"Rp"}
                        chartConfig={{
                        backgroundColor: "yellow",
                        backgroundGradientFrom: "yellow",
                        backgroundGradientTo: "yellow",
                        decimalPlaces: 2, // optional, defaults to 2dp
                        color: (opacity = 1) => `black`,
                        labelColor: (opacity = 1) => `black`,
                        barPercentage:1,
                        style: {
                            borderRadius: 16
                        }
                        }}
                        style={{
                        marginVertical: 8,
                        borderRadius: 16
                        }}
                    />
                </Container>
          );
      }
  }