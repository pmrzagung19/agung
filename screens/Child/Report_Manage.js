import React, { Fragment } from "react";
import {
  Text,
  View,
  TextInput,
  TouchableHighlight,
  StyleSheet,
  ImageBackground
} from "react-native";
import { Container, Content, Col, Button, Thumbnail,Icon, Row, Card, CardItem, Body, Accordion, Picker, Form, Item, Label } from "native-base";
import {
  LineChart,
  BarChart,
  PieChart,
  ProgressChart,
  ContributionGraph,
  StackedBarChart
} from "react-native-chart-kit";
import { Dimensions } from "react-native";
import Swiper from 'react-native-swiper'

const screenWidth = Dimensions.get("window").width;

const dataArray1 = [
    { title: "2 minutes per server", content: "Lorem ipsum dolor sit amet" }
  ];
const dataArray2 = [
    { title: "4 customers per minute", content: "Lorem ipsum dolor sit amet" }
  ];
const dataArray3 = [
    { title: "8 customers", content: "Lorem ipsum dolor sit amet" }
  ];
const dataArray4 = [
    { title: "26 customers per day", content: "Lorem ipsum dolor sit amet" }
  ];

class Welcome extends React.Component {

  _renderHeader(item, expanded) {
    return (
      <View style={{
        flexDirection: "row",
        padding: 10,
        justifyContent: "space-between",
        alignItems: "center" ,
        backgroundColor: "white" }}>
        <Text style={{ fontWeight: "600",fontSize:16 }}>
          {" "}{item.title}
        </Text>
        {expanded
          ? <Icon style={{ fontSize: 18 }} name="arrow-up" type='SimpleLineIcons' />
          : <Icon style={{ fontSize: 18 }} name="arrow-down" type='SimpleLineIcons' />}
      </View>
    );
  }
  _renderContent(item) {
    return (
      <View style={{flex:1,flexDirection:'row',justifyContent:'space-between',borderWidth:0}}>
        <View style={{flex:3}}>
        <Text
          style={{
            padding: 10,
            fontStyle: "italic",
          }}
        >
          {item.content}
        </Text>
        </View>
        <View style={{flex:1,justifyContent:'center',alignItems:'flex-end'}}>
        <Picker style={{position: "absolute", top: 0,right:10, width: 90,height:1000,justifyContent:'center'}} mode='dropdown'>
          <Picker.Item label="Call" value="Call"/>
          <Picker.Item label="Remove" value="Remove"/>
          <Picker.Item label="Transfer" value="Transfer" />
        </Picker>
        <Icon style={{alignItems:'flex-end',color:'black'}} name='dots-vertical' type='MaterialCommunityIcons'/>
        </View>
      </View>
    );
  }
  _renderChart(item) {
    return (
      <Swiper style={{height:200}} showsButtons={true} showsPagination={false}>
        <View style={styles.slide1}>
          <StackedBarChart
            data={{
            labels: ["Test1", "Test2"],
            legend: ["L1", "L2", "L3"],
            data: [[60, 60, 60], [30, 30, 60]],
            barColors: ["#dfe4ea", "#ced6e0", "#a4b0be"]
            }}
            width={Dimensions.get("window").width -100} // from react-native
            height={190}
            yAxisLabel={"Rp"}
            chartConfig={{
            backgroundGradientFrom: "",
            backgroundGradientTo: "white",
            decimalPlaces: 2, // optional, defaults to 2dp
            color: (opacity = 1) => `black`,
            labelColor: (opacity = 1) => `black`,
            style: {
            borderRadius: 16
            }
            }}
            style={{
            borderRadius: 16
            }}
          />
        </View>
        <View style={styles.slide2}>
          <BarChart
            data={{
            labels: ["Januari", "Februari", "Maret", "April", "Mei", "Juni"],
            datasets: [
              {
                data: [
                  Math.random() * 100,
                  Math.random() * 100,
                  Math.random() * 100,
                  Math.random() * 100,
                  Math.random() * 100,
                  Math.random() * 100
                ]
              }
             ]
            }}
            width={Dimensions.get("window").width - 100} // from react-native
            height={200}
            yAxisLabel={"Rp"}
            chartConfig={{
            backgroundColor: "",
            backgroundGradientFrom: "",
            backgroundGradientTo: "white",
            decimalPlaces: 2, // optional, defaults to 2dp
            color: (opacity = 1) => `black`,
            labelColor: (opacity = 1) => `black`,
            barPercentage:1,
            style: {
            borderRadius: 16
            }
            }}
          />
        </View>
      </Swiper>
    );
  }
  render() {
    const{navigate}=this.props.navigation;
    return (
      <Container >
      <Content contentContainerStyle={{flex:1,margin:20}}>
        <Text style={{}}>Number of servers</Text>
        <Card>
            <CardItem>
                <Body>
                    <Text style={{fontSize:16}}>8 server(s)</Text>
                </Body>
            </CardItem>
        </Card>
          <Content contentContainerStyle={{marginHorizontal:1}}>
          <Text style={{marginVertical:10}}>Service Rate</Text>
            <Accordion
              style={{marginBottom:10,borderWidth:0.5}}
              dataArray={dataArray1}
              animation={true}
              expanded={true}
              renderHeader={this._renderHeader}
              renderContent={this._renderContent}
            />
            <Text style={{marginVertical:10}}>Customer arrival rate</Text>
            <Accordion
              style={{marginBottom:10,borderWidth:0.5}}
              dataArray={dataArray2}
              animation={true}
              expanded={true}
              renderHeader={this._renderHeader}
              renderContent={this._renderContent}
            />
            <Text style={{marginVertical:10}}>Maximum queue capacity</Text>
            <Accordion
              style={{marginBottom:10,borderWidth:0.5}}
              dataArray={dataArray3}
              animation={true}
              expanded={true}
              renderHeader={this._renderHeader}
              renderContent={this._renderContent}
            />
            <Text style={{marginVertical:10}}>Customer Population</Text>
            <Accordion
              style={{marginBottom:10,borderWidth:0.5}}
              dataArray={dataArray4}
              animation={true}
              expanded={true}
              renderHeader={this._renderHeader}
              renderContent={this._renderChart}
            />
          </Content>
      </Content>
    </Container>
    );
  }
}

export default Welcome;

const styles = StyleSheet.create({
    Accordion:{
        elevation: 10,
        marginBottom:20
    },
    col1: {
      alignItems:'center',
    },
    sf5bacfa4: {
      fontSize: 25,
      textAlign: `center`,
      fontWeight: `400`
    },
    sf6f58cc7: {
      flex: 1,
      flexDirection: `column`,
      justifyContent: `center`
    },
    s28bd15d1: {
      fontSize: 25,
      width: `90%`,
      fontWeight: `400`,
      borderBottomWidth: 3,
      borderBottomColor: `rgba(0, 145, 95, 1)`,
      paddingLeft: 10
    },
    s1157eaee: {
      flex: 3,
      justifyContent: `center`
    },
    s4bb7eeaf: {
      flexDirection: `row`,
      justifyContent: `center`,
      alignItems: `stretch`,
      marginBottom: 20
    },
    s44f53c6e: {
      fontSize: 11,
      fontWeight: `400`,
      color: `rgba(0, 145, 95, 1)`,
      marginBottom: 20
    },
    s29adf86c: {
      textAlign: `center`,
      fontSize: 20,
      color: `rgba(255, 255, 255, 1)`,
      fontWeight: `400`
    },
    scf3d97cc: {
      backgroundColor: `rgba(0, 145, 95, 1)`,
      justifyContent: `center`,
      borderRadius: 75,
      alignItems: `stretch`,
      height: 55,
      borderColor: 'white',
      borderWidth: 1
    },
    s59ba8be0: {
      justifyContent: `center`,
      alignItems: `stretch`,
      marginBottom: 2,
      marginTop: 4
    },
    s48aa0650: {
      flex: 4,
      alignItems: `stretch`,
      flexDirection: `column`,
      padding: 20
    },
    s48aa0651: {
        flex: 1,
        alignItems: `stretch`,
        flexDirection: `column`,
        padding: 20
      },
      spinner: {
        width: "100%",
        paddingHorizontal:10
      },
    s13f64db2: {
      flex: 1,
      alignItems: `stretch`,
      flexDirection: `column`,
      backgroundColor: `rgba(255, 255, 255, 1)`
    },wrapper: {},
    slide1: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    slide2: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    slide3: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    text: {
      color: '#fff',
      fontSize: 30,
      fontWeight: 'bold'
    }
  });