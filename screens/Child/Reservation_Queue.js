import React, { Fragment } from "react";
import {
  Text,
  View,
  TextInput,
  TouchableHighlight,
  StyleSheet,
  Dimensions,
  ImageBackground
} from "react-native";
import { Container, Content, Col, Button, Thumbnail, Icon, Row } from "native-base";
import SwipeButton from 'rn-swipe-button';
import { RNSlidingButton, SlideDirection } from 'rn-sliding-button';

var SCREEN_WIDTH = Dimensions.get('window').width;

class Welcome extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      swiped: false,
      leftSwiped: false,
      rightSwiped: false,
    };
  }

  onSlideRight = () => {
    this.props.navigation.navigate('Reservation_Daftar')
  };

  onLeftSlide() {
    var self = this;
    this.setState({ swiped: true, leftSwiped: true }, () => {
      setTimeout(() => self.setState({ swiped: false, leftSwiped: false }), 2500);
    });
  }

  onRightSlide() {
    var self = this;
    this.setState({ swiped: true, rightSwiped: true }, () => {
      setTimeout(() => self.setState({ swiped: false, rightSwiped: false }), 2500);
    });
  }

  onBothSlide() {
    var self = this;
    this.setState({ swiped: true, bothSwiped: true }, () => {
      setTimeout(() => self.setState({ swiped: false, bothSwiped: false }), 2500);
    });
  }
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={{ flex: 1 }}>
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', padding: 30 }}>
          <Thumbnail
            source={require('./sushi.jpg')}
            square
            style={{ height: 200, width: 200 }}
          />
          <Text>Sushi Tei</Text>
          <Text>Plaza Indonesia, 3rd floor</Text>
        </View>
        <View style={{ height: 100, paddingHorizontal: 30, flexDirection: 'row', marginBottom: 20 }}>
          <View style={{ flex: 1, borderWidth: 2, borderColor: '#009371', borderTopLeftRadius: 10, borderBottomLeftRadius: 10, flexDirection: 'column', alignItems: 'center', justifyContent: 'space-evenly' }}>
            <Text style={{ color: '#009371', fontSize: 17 }}>Now serving</Text>
            <Text style={{ color: '#009371', fontSize: 30, fontWeight: 'bold' }}>Q031</Text>
          </View>
          <View style={{ flex: 1, borderBottomWidth: 2, borderTopWidth: 2, borderColor: '#009371', flexDirection: 'column', alignItems: 'center', justifyContent: 'space-evenly' }}>
            <Text style={{ color: '#009371', fontSize: 17 }}>Pax</Text>
            <Text style={{ color: '#009371', fontSize: 30, fontWeight: 'bold' }}>6</Text>
          </View>
          <View style={{ flex: 1, borderWidth: 2, borderColor: '#009371', borderTopRightRadius: 10, borderBottomRightRadius: 10, flexDirection: 'column', alignItems: 'center', justifyContent: 'space-evenly' }}>
            <Text style={{ color: '#009371', fontSize: 17 }}>Counter</Text>
            <Text style={{ color: '#009371', fontSize: 30, fontWeight: 'bold' }}>1</Text>
          </View>
        </View>
        <View style={{ flex: 0.7, justifyContent: 'center', alignItems: 'center' }}>
          <RNSlidingButton
            style={{
              width: 320,
              borderRadius: 30,
              backgroundColor: '#009371'
            }}
            height={65}
            onSlidingSuccess={this.onSlideRight}
            slideDirection={SlideDirection.RIGHT}>
            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
              <View style={{ height: 45, width: 45, backgroundColor: 'white', marginRight: 15, justifyContent: 'center', alignItems: 'center', borderRadius: 30 }}>
                <Icon
                  name='ios-arrow-forward' type='Ionicons'
                  style={{ color: '#009371' }}
                />
              </View>
              <Text numberOfLines={1} style={{ color: 'white', fontSize: 16 }}>
                Slide to call next customer
                </Text>
            </View>
          </RNSlidingButton>
        </View>
      </View>
    );
  }
}

export default Welcome;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonOuter: {
    marginTop: 20
  },
  buttonInner: {
    width: SCREEN_WIDTH - 40,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center'
  },
  button: {
    color: 'white',
    fontSize: 15,
    fontWeight: 'bold'
  },
  col1: {
    alignItems: 'center',
  },
  sf5bacfa4: {
    fontSize: 25,
    textAlign: `center`,
    fontWeight: `400`
  },
  sf6f58cc7: {
    flex: 1,
    flexDirection: `column`,
    justifyContent: `center`
  },
  s28bd15d1: {
    fontSize: 25,
    width: `90%`,
    fontWeight: `400`,
    borderBottomWidth: 3,
    borderBottomColor: `rgba(0, 145, 95, 1)`,
    paddingLeft: 10
  },
  s1157eaee: {
    flex: 3,
    justifyContent: `center`
  },
  s4bb7eeaf: {
    flexDirection: `row`,
    justifyContent: `center`,
    alignItems: `stretch`,
    marginBottom: 20
  },
  s44f53c6e: {
    fontSize: 11,
    fontWeight: `400`,
    color: `rgba(0, 145, 95, 1)`,
    marginBottom: 20
  },
  s29adf86c: {
    textAlign: `center`,
    fontSize: 20,
    color: `rgba(255, 255, 255, 1)`,
    fontWeight: `400`
  },
  scf3d97cc: {
    backgroundColor: `rgba(0, 145, 95, 1)`,
    justifyContent: `center`,
    borderRadius: 75,
    alignItems: `stretch`,
    height: 55,
    borderColor: 'white',
    borderWidth: 1
  },
  s59ba8be0: {
    justifyContent: `center`,
    alignItems: `stretch`,
    marginBottom: 2,
    marginTop: 4
  },
  s48aa0650: {
    flex: 4,
    alignItems: `stretch`,
    flexDirection: `column`,
    padding: 20
  },
  s48aa0651: {
    flex: 1,
    alignItems: `stretch`,
    flexDirection: `column`,
    padding: 20
  },
  s13f64db2: {
    flex: 1,
    alignItems: `stretch`,
    flexDirection: `column`,
    backgroundColor: `rgba(255, 255, 255, 1)`
  }
});