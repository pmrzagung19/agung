import React, { Fragment } from "react";
import {
  Text,
  TextInput,
  View,
  TouchableHighlight,
  ScrollView,
  StyleSheet
} from "react-native";
import { SafeAreaView } from "react-navigation";
import { Container, Content, Input, Form, Item, Label, Icon, Button, Thumbnail, ListItem, Left, Right, Radio,Picker } from "native-base";
import PasswordInputText from 'react-native-hide-show-password-input';
import { HelperText} from 'react-native-paper';
import RNPickerSelect from 'react-native-picker-select';


const styles = StyleSheet.create({
  sfca3b6fb: {
    fontWeight: `500`,
    fontSize: 25,
    marginBottom: 10
  },
  s1054bc31: {
    marginBottom: 10,
    color: `rgba(0, 0, 0, 1)`
  },
  sf16512cb: {
    fontSize: 20,
    fontWeight: `400`,
    borderBottomWidth: 3,
    borderBottomColor: `rgba(0, 145, 95, 1)`,
    textAlign: `left`
  },
  s0905a6f3: {
    justifyContent: `flex-start`,
    marginBottom: 10
  },
  s18cbd0b5: {
    marginBottom: 10,
    color: `rgba(0, 0, 0, 1)`
  },
  s7d8d8586: {
    fontSize: 20,
    fontWeight: `400`,
    borderBottomWidth: 3,
    borderBottomColor: `rgba(0, 145, 95, 1)`,
    textAlign: `left`
  },
  se97c925a: {
    justifyContent: `flex-start`,
    marginBottom: 10
  },
  s84f75f76: {
    marginBottom: 10,
    color: `rgba(0, 0, 0, 1)`
  },
  s9384d1a1: {
    fontSize: 20,
    fontWeight: `400`,
    borderBottomWidth: 3,
    borderBottomColor: `rgba(0, 145, 95, 1)`,
    textAlign: `left`
  },
  s18772e5f: {
    justifyContent: `flex-start`,
    marginBottom: 10
  },
  se66991ab: {
    marginBottom: 10,
    color: `rgba(0, 0, 0, 1)`
  },
  s36b417b0: {
    fontSize: 20,
    fontWeight: `400`,
    borderBottomWidth: 3,
    borderBottomColor: `rgba(0, 145, 95, 1)`,
    textAlign: `left`
  },
  sf734a2f7: {
    justifyContent: `flex-start`,
    marginBottom: 10
  },
  see86fb68: {
    marginBottom: 10,
    color: `rgba(0, 0, 0, 1)`
  },
  s5b0ad781: {
    justifyContent: `flex-start`,
    marginBottom: 10
  },
  s301174cb: {
    marginBottom: 10,
    color: `rgba(0, 0, 0, 1)`
  },
  sb70b07ca: {
    fontSize: 20,
    fontWeight: `400`,
    borderBottomWidth: 3,
    borderBottomColor: `rgba(0, 145, 95, 1)`,
    textAlign: `left`
  },
  sfb3e5edf: {
    justifyContent: `flex-start`,
    marginBottom: 50
  },
  s56e8ed08: {
    textAlign: `center`,
    fontSize: 20,
    color: `rgba(255, 255, 255, 1)`,
    fontWeight: `400`
  },
  s7fd2bed2: {
    backgroundColor: `rgba(0, 145, 95, 1)`,
    justifyContent: `center`,
    borderRadius: 75,
    height: 55
  },
  scd1d1d72: {
    flex: 1,
    alignItems: `stretch`,
    flexDirection: `column`,
    justifyContent: `flex-end`,
    marginTop: 125
  },
  s9eedc2ea: {
    flex: 2,
    alignItems: `stretch`,
    flexDirection: `column`,
    padding: 20,
    justifyContent: `flex-start`
  },
  sefd4c879: {
    flex: 1,
    alignItems: `stretch`,
    flexDirection: `column`
  }
});

const _05 = props => {
  const { navigation } = props;

  
};

class Form_Profile extends React.Component{
  
  constructor(props) {
    super(props);

    this.inputRefs = {
      firstTextInput: null,
      favSport0: null,
      favSport1: null,
      lastTextInput: null,
      favSport5: null,
    };

    this.state = {
      numbers: [
        {
          label: '1',
          value: 1,
          color: 'orange',
        },
        {
          label: '2',
          value: 2,
          color: 'green',
        },
      ],
      favSport0: undefined,
      favSport1: undefined,
      favSport2: undefined,
      favSport3: undefined,
      favSport4: 'baseball',
      previousFavSport5: undefined,
      favSport5: null,
      favNumber: undefined,
    };
  }
  render(){
    const{icon} = this.props;
    const{navigate} = this.props.navigation;
    return (
        <Container>
            <View style={{flex:1}}>
              <View style={{flex:3.3,padding:15}}>
                <Content showsVerticalScrollIndicator={false}>
                    <Form>
                        <Item floatingLabel>
                            <Label style={{fontSize:15}}>Store / outlet / branch name</Label>
                        </Item>
                        <Item floatingLabel>
                            <Label style={{fontSize:15}}>Industry</Label>
                        </Item>
                        <Item floatingLabel>
                            <Label style={{fontSize:15}}>Phone number</Label>
                        </Item>
                        <Item floatingLabel>
                            <Label style={{fontSize:15}}>Area name</Label>
                        </Item>
                        <Content contentContainerStyle={{marginLeft:10,marginTop:10,borderColor:'#9EA0A4',borderBottomWidth:1}}>
                        <RNPickerSelect
                              placeholder={{label:'Country',value:null,color: '#9EA0A4'}}
                              style={pickerSelectStyles}
                              onValueChange={(value) => console.log(value)}
                              items={[
                                  { label: 'Indonesia', value: 'Indonesia' },
                                  { label: 'Singapura', value: 'Singapura' },
                                  { label: 'Australia', value: 'Australia' },
                              ]}
                          />
                        </Content>
                        <Content contentContainerStyle={{marginLeft:10,marginVertical:10,borderColor:'#9EA0A4',borderBottomWidth:1}}>
                        <RNPickerSelect
                              placeholder={{label:'Province',value:null,color: '#9EA0A4'}}
                              style={pickerSelectStyles}
                              onValueChange={(value) => console.log(value)}
                              items={[
                                  { label: 'Jawa', value: 'Jawa' },
                                  { label: 'Kalimantan', value: 'Kalimantan' },
                                  { label: 'Sumatera', value: 'Sumatera' },
                              ]}
                          />
                        </Content>
                        <Content contentContainerStyle={{marginLeft:10,borderColor:'#9EA0A4',borderBottomWidth:1}}>
                          <RNPickerSelect
                              placeholder={{label:'City',value:null,color: '#9EA0A4'}}
                              style={pickerSelectStyles}
                              onValueChange={(value) => console.log(value)}
                              items={[
                                  { label: 'Tangerang', value: 'Tangerang' },
                                  { label: 'Surabaya', value: 'Surabaya' },
                                  { label: 'Bandung', value: 'Bandung' },
                              ]}
                          />
                        </Content>
                        <Item floatingLabel>
                            <Label style={{fontSize:15}}>Street address</Label>
                        </Item>
                        <Item floatingLabel>
                            <Label style={{fontSize:15}}>Postal code</Label>
                        </Item>
                    </Form>
                </Content>
                </View>
                <View style={{flex:1,justifyContent:'center',paddingHorizontal:20}}>
                    <Button onPress={()=>this.props.navigation.goBack()} style={{borderRadius:20,backgroundColor:'#009371',justifyContent:'center'}}>
                        <Text style={{fontSize:18,fontWeight:'bold',color:'white'}}>Save</Text>
                    </Button>
                </View>
            </View>
        </Container>
   );
  }
}

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 16,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: 'purple',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
});

export default Form_Profile;

export { styles };
