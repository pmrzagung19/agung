import React, { Fragment } from "react";
import {
    Text,
    View,
    TextInput,
    TouchableHighlight,
    StyleSheet,
    ImageBackground,
    ScrollView
} from "react-native";
import { Container, Content, Col, Button, Thumbnail, Icon, Row, Card, CardItem, Body, Accordion, Picker, Form, Item, Label, Fab, Left, Right, Image } from "native-base";
import { withNavigation } from 'react-navigation';
import Modal from "react-native-modal";
import InputSpinner from "react-native-input-spinner";
import Footer from "./Footer";
import { RNSlidingButton, SlideDirection } from 'rn-sliding-button';
import { connect } from 'react-redux';

class Myevent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            swiped: false,
            leftSwiped: false,
            rightSwiped: false,
            isModalVisible: true
        };

        super(props)
        this.state = {
            active: false
        };
    }

    onSlideRight = () => {
        this.props.navigation.navigate('routeSettings')
    };

    onLeftSlide() {
        var self = this;
        this.setState({ swiped: true, leftSwiped: true }, () => {
            setTimeout(() => self.setState({ swiped: false, leftSwiped: false }), 2500);
        });
    }

    onRightSlide() {
        var self = this;
        this.setState({ swiped: true, rightSwiped: true }, () => {
            setTimeout(() => self.setState({ swiped: false, rightSwiped: false }), 2500);
        });
    }

    onBothSlide() {
        var self = this;
        this.setState({ swiped: true, bothSwiped: true }, () => {
            setTimeout(() => self.setState({ swiped: false, bothSwiped: false }), 2500);
        });
    }

    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
    };

    toggleModal1 = () => {
        this.setState({ isModalVisible1: !this.state.isModalVisible1 });
    };

    render() {
        const { navigate } = this.props.navigation;
        return (
            <Container>
                <ScrollView>
                    <Content contentContainerStyle={{ flex: 1, marginHorizontal: 20, marginTop: 10, marginBottom: 25 }}>
                        <Card>
                            <CardItem>
                                <Body>
                                    <Text style={{ marginBottom: 5, fontSize: 20, color: 'blue' }}>1.
                                <Text style={{ fontWeight: 'bold', fontSize: 20, color: 'black' }}> Jakcloth
                                </Text>
                                    </Text>
                                    <Text style={{ marginBottom: 20, fontSize: 20 }}>Jum'at,
                                <Text style={{ fontWeight: 'bold', fontSize: 20 }}> 03 April 2020
                                </Text>
                                    </Text>
                                    <Text style={{ fontSize: 15 }}>Waiting for confirmation
                                <Text style={{ fontSize: 20 }}> ● </Text>
                                        <Text style={{ color: 'red', fontSize: 15 }}>Second calling</Text>
                                    </Text>
                                    <View style={{ flexDirection: 'row', marginVertical: 10, marginTop: 20, justifyContent: 'center' }}>
                                        <Button onPress={() => navigate('Reservation_Queue')} style={{ flex: 1, marginRight: 20, borderRadius: 20, backgroundColor: '#009371', justifyContent: 'center' }}>
                                            <Text style={{ fontSize: 18, fontWeight: 'bold', color: 'white' }}>Mulai Event</Text>
                                        </Button>
                                    </View>
                                </Body>
                            </CardItem>
                        </Card>

                        <Card>
                            <CardItem>
                                <Body>
                                    <Text style={{ marginBottom: 5, fontSize: 20, color: 'blue' }}>2.
                                <Text style={{ fontWeight: 'bold', fontSize: 20, color: 'black' }}> BTS
                                </Text>
                                    </Text>
                                    <Text style={{ marginBottom: 20, fontSize: 20 }}>Jum'at,
                                <Text style={{ fontWeight: 'bold', fontSize: 20 }}> 03 Mei 2020
                                </Text>
                                    </Text>
                                    <Text style={{ fontSize: 15 }}>Waiting for confirmation
                                <Text style={{ fontSize: 20 }}> ● </Text>
                                        <Text style={{ color: 'red', fontSize: 15 }}>Second calling</Text>
                                    </Text>
                                    <View style={{ flexDirection: 'row', marginVertical: 10, marginTop: 20, justifyContent: 'center', position: 'relative' }}>
                                        <Button onPress={() => navigate('Reservation_Queue')} style={{ flex: 1, marginRight: 20, borderRadius: 20, backgroundColor: '#009371', justifyContent: 'center' }}>
                                            <Text style={{ fontSize: 18, fontWeight: 'bold', color: 'white' }}>Mulai Event</Text>
                                        </Button>
                                    </View>
                                </Body>
                            </CardItem>
                        </Card>

                        <Card>
                            <CardItem>
                                <Body>
                                    <Text style={{ marginBottom: 5, fontSize: 20, color: 'blue' }}>3.
                                        <Text style={{ fontWeight: 'bold', fontSize: 20, color: 'black' }}> BLACKPINK
                                        </Text>
                                    </Text>
                                    <Text style={{ marginBottom: 20, fontSize: 20 }}>Jum'at,
                                        <Text style={{ fontWeight: 'bold', fontSize: 20 }}> 03 Juni 2020
                                        </Text>
                                    </Text>
                                    <Text style={{ fontSize: 15 }}>Waiting for confirmation
                                        <Text style={{ fontSize: 20 }}> ● </Text>
                                        <Text style={{ color: 'red', fontSize: 15 }}>Second calling</Text>
                                    </Text>
                                    <View style={{ flexDirection: 'row', marginVertical: 10, marginTop: 20, justifyContent: 'center' }}>
                                        <Button onPress={() => navigate('Reservation_Queue')} style={{ flex: 1, marginRight: 20, borderRadius: 20, backgroundColor: '#009371', justifyContent: 'center' }}>
                                            <Text style={{ fontSize: 18, fontWeight: 'bold', color: 'white' }}>Mulai Event</Text>
                                        </Button>
                                    </View>
                                </Body>
                            </CardItem>
                        </Card>

                        <View style={{ position: "absolute" }}>
                            <Fab
                                active={this.state.active}
                                direction="up"
                                containerStyle={{}}
                                style={{ backgroundColor: '#5067FF' }}
                                position="bottomRight"
                                onPress={() => this.setState({ active: !this.state.active })}>
                                <Icon name="add" />

                            </Fab>
                        </View>
                    </Content>
                </ScrollView>
            </Container>
        );
    }
}


const mapStateToProps = ({ auth }) => {
    const { email } = auth;

    return { email };
};

export default connect(mapStateToProps)(withNavigation(Myevent));

const styles = StyleSheet.create({
    Accordion: {
        elevation: 10,
        marginBottom: 20
    },
    col1: {
        alignItems: 'center',
    },
    sf5bacfa4: {
        fontSize: 25,
        textAlign: `center`,
        fontWeight: `400`
    },
    sf6f58cc7: {
        flex: 1,
        flexDirection: `column`,
        justifyContent: `center`
    },
    s28bd15d1: {
        fontSize: 25,
        width: `90%`,
        fontWeight: `400`,
        borderBottomWidth: 3,
        borderBottomColor: `rgba(0, 145, 95, 1)`,
        paddingLeft: 10
    },
    s1157eaee: {
        flex: 3,
        justifyContent: `center`
    },
    s4bb7eeaf: {
        flexDirection: `row`,
        justifyContent: `center`,
        alignItems: `stretch`,
        marginBottom: 20
    },
    s44f53c6e: {
        fontSize: 11,
        fontWeight: `400`,
        color: `rgba(0, 145, 95, 1)`,
        marginBottom: 20
    },
    s29adf86c: {
        textAlign: `center`,
        fontSize: 20,
        color: `rgba(255, 255, 255, 1)`,
        fontWeight: `400`
    },
    scf3d97cc: {
        backgroundColor: `rgba(0, 145, 95, 1)`,
        justifyContent: `center`,
        borderRadius: 75,
        alignItems: `stretch`,
        height: 55,
        borderColor: 'white',
        borderWidth: 1
    },
    s59ba8be0: {
        justifyContent: `center`,
        alignItems: `stretch`,
        marginBottom: 2,
        marginTop: 4
    },
    s48aa0650: {
        flex: 4,
        alignItems: `stretch`,
        flexDirection: `column`,
        padding: 20
    },
    s48aa0651: {
        flex: 1,
        alignItems: `stretch`,
        flexDirection: `column`,
        padding: 20
    },
    spinner: {
        width: "100%",
        paddingHorizontal: 10
    },
    s13f64db2: {
        flex: 1,
        alignItems: `stretch`,
        flexDirection: `column`,
        backgroundColor: `rgba(255, 255, 255, 1)`
    }
});