import React, { Fragment } from "react";
import {
  Text,
  View,
  TextInput,
  TouchableHighlight,
  StyleSheet,
  ImageBackground
} from "react-native";
import { SafeAreaView } from "react-navigation";
import LinearGradient from 'react-native-linear-gradient';
import { Container, Content, Col, Button, Thumbnail } from "native-base";

class Welcome extends React.Component {

  onSlideRight = () => {
    //perform Action on slide success.
};

  render() {
    const{navigate}=this.props.navigation;
    return (
        <Content contentContainerStyle={{flex:1,padding:35,paddingVertical:100}}>
           <Content contentContainerStyle={{height:350,padding:15,backgroundColor:'#009371',borderRadius:20,alignItems:'center',justifyContent:'space-around'}}>
                    <Thumbnail
                        source={require('./frame.png')}
                        square
                        style={{height:220,width:220,margin:10}}
                    />
                    <Text style={{color:'white',fontWeight:'bold',fontSize:20}}>Sushi Tei Plaza Indonesia</Text>
                </Content>
        </Content>
    );
  }
}

export default Welcome;

const styles = StyleSheet.create({
    col1: {
      alignItems:'center',
    },
    sf5bacfa4: {
      fontSize: 25,
      textAlign: `center`,
      fontWeight: `400`
    },
    sf6f58cc7: {
      flex: 1,
      flexDirection: `column`,
      justifyContent: `center`
    },
    s28bd15d1: {
      fontSize: 25,
      width: `90%`,
      fontWeight: `400`,
      borderBottomWidth: 3,
      borderBottomColor: `rgba(0, 145, 95, 1)`,
      paddingLeft: 10
    },
    s1157eaee: {
      flex: 3,
      justifyContent: `center`
    },
    s4bb7eeaf: {
      flexDirection: `row`,
      justifyContent: `center`,
      alignItems: `stretch`,
      marginBottom: 20
    },
    s44f53c6e: {
      fontSize: 11,
      fontWeight: `400`,
      color: `rgba(0, 145, 95, 1)`,
      marginBottom: 20
    },
    s29adf86c: {
      textAlign: `center`,
      fontSize: 20,
      color: `rgba(255, 255, 255, 1)`,
      fontWeight: `400`
    },
    scf3d97cc: {
      backgroundColor: `rgba(0, 145, 95, 1)`,
      justifyContent: `center`,
      borderRadius: 75,
      alignItems: `stretch`,
      height: 55,
      borderColor: 'white',
      borderWidth: 1
    },
    s59ba8be0: {
      justifyContent: `center`,
      alignItems: `stretch`,
      marginBottom: 2,
      marginTop: 4
    },
    s48aa0650: {
      flex: 4,
      alignItems: `stretch`,
      flexDirection: `column`,
      padding: 20
    },
    s48aa0651: {
        flex: 1,
        alignItems: `stretch`,
        flexDirection: `column`,
        padding: 20
      },
    s13f64db2: {
      flex: 1,
      alignItems: `stretch`,
      flexDirection: `column`,
      backgroundColor: `rgba(255, 255, 255, 1)`
    }
  });