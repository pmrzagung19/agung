import React, { Fragment } from "react";
import { Text, TextInput, View, StyleSheet, TouchableHighlight } from "react-native";
import { SafeAreaView } from "react-navigation";

const styles = StyleSheet.create({
    scd1d1d72: {
        flex: 1,
        alignItems: `stretch`,
        flexDirection: `column`,
        justifyContent: `flex-end`,
        marginTop: 125
      },
      s7fd2bed2: {
        backgroundColor: `rgba(0, 145, 95, 1)`,
        justifyContent: `center`,
        borderRadius: 75,
        height: 55
      },
      s56e8ed08: {
        textAlign: `center`,
        fontSize: 20,
        color: `rgba(255, 255, 255, 1)`,
        fontWeight: `400`
      },
  s8dc57587: {
    fontSize: 23,
    fontWeight: `500`
  },
  s289d39c4: {
    width: 70,
    marginRight: 10,
    textAlign: `center`,
    borderWidth: 3,
    borderRadius: 10,
    borderColor: `rgba(0, 145, 95, 1)`,
    fontSize: 50
  },
  s7a626c63: {
    width: 70,
    marginRight: 10,
    textAlign: `center`,
    borderWidth: 3,
    borderRadius: 10,
    borderColor: `rgba(0, 145, 95, 1)`,
    fontSize: 50
  },
  s14a3c49d: {
    width: 70,
    marginRight: 10,
    textAlign: `center`,
    borderWidth: 3,
    borderRadius: 10,
    borderColor: `rgba(0, 145, 95, 1)`,
    fontSize: 50
  },
  sb044d453: {
    width: 70,
    marginRight: 10,
    textAlign: `center`,
    borderWidth: 3,
    borderRadius: 10,
    borderColor: `rgba(0, 145, 95, 1)`,
    fontSize: 50
  },
  s5be83475: {
    flexDirection: `row`,
    justifyContent: `center`,
    padding: 20
  },
  s71f86d6f: {
    textAlign: `center`
  },
  s3414a2d0: {
    textDecorationLine: `underline`,
    color: `rgba(0, 145, 95, 1)`,
    fontWeight: `500`,
    textAlign: `center`
  },
  s05da1b36: {
    flex: 2,
    padding: 20
  },
  s79a0981b: {
    flex: 1,
    alignItems: `stretch`,
    flexDirection: `column`
  }
});

const _02 = props => {
  const { navigation } = props;

  return (
    <Fragment>
      <SafeAreaView style={styles.s79a0981b}>
        <View style={styles.s05da1b36}>
          <Text style={styles.s8dc57587}>
            Enter verification code we've just send you.
          </Text>
          <View style={styles.s5be83475}>
            <TextInput
              placeholder={`1`}
              keyboardType={`numeric`}
              style={styles.s289d39c4}
            />
            <TextInput
              placeholder={`2`}
              keyboardType={`numeric`}
              style={styles.s7a626c63}
            />
            <TextInput
              placeholder={`3`}
              keyboardType={`numeric`}
              style={styles.s14a3c49d}
            />
            <TextInput
              placeholder={`4`}
              keyboardType={`numeric`}
              style={styles.sb044d453}
            />
          </View>
          <Text style={styles.s71f86d6f}>Time remaining 01:30</Text>
          <Text
            onPress={() => {
              props.navigation.navigate("03");
            }}
            style={styles.s3414a2d0}
          >
            Resend
          </Text>
          <View style={styles.scd1d1d72}>
              <TouchableHighlight
                onPress={() => {
                  props.navigation.navigate("Thanks");
                }}
                style={styles.s7fd2bed2}
              >
                <Text style={styles.s56e8ed08}>Continue</Text>
              </TouchableHighlight>
            </View>
        </View>
      </SafeAreaView>
    </Fragment>
  );
};

_02.defaultProps = {};

export default _02;

export { styles };
