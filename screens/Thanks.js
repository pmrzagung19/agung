import React, { Fragment } from "react";
import { Image, Text, View, StyleSheet, TouchableHighlight } from "react-native";
import imgScreenShot20190917At164453 from "../assets/bide-empty.png";
import { SafeAreaView } from "react-navigation";

const styles = StyleSheet.create({
    scd1d1d72: {
        flex: 1,
        alignItems: `stretch`,
        flexDirection: `column`,
        justifyContent: `flex-end`,
        marginTop: 125
      },
      s7fd2bed2: {
        backgroundColor: `rgba(0, 145, 95, 1)`,
        justifyContent: `center`,
        borderRadius: 75,
        height: 55
      },
      s56e8ed08: {
        textAlign: `center`,
        fontSize: 20,
        color: `rgba(255, 255, 255, 1)`,
        fontWeight: `400`
      },
  s8b64a333: {
    width: 150,
    height: 150
  },
  s7181f2a4: {
    fontSize: 25,
    textAlign: `center`,
    fontWeight: `bold`
  },
  s7181f2a5: {
    fontSize: 18,
    textAlign: `center`,
    fontWeight: `400`
  },
  s8663515c: {
    flex: 3,
    alignItems: `center`,
    flexDirection: `column`,
    padding: 20,
    justifyContent: `center`
  },
  s8663515d: {
    flex: 1,
    alignItems: `stretch`,
    flexDirection: `column`,
    padding: 20,
    justifyContent: `center`
  },
  s1b804948: {
    flex: 1,
    alignItems: `stretch`,
    flexDirection: `column`
  }
});

const _08 = props => {
  return (
    <Fragment>
      <SafeAreaView style={styles.s1b804948}>
        <View style={styles.s8663515c}>
          <Image
            style={styles.s8b64a333}
            source={imgScreenShot20190917At164453}
          />
          <Text style={styles.s7181f2a4}>
            Thank you for using Bide!
          </Text>
          <Text style={styles.s7181f2a5}>
            You will be required to fill out your business profile before you can use the full features of this queueing system
          </Text>
        </View>
        <View style={styles.s8663515d}>
          <View style={styles.scd1d1d72}>
              <TouchableHighlight
                onPress={() => {
                  props.navigation.navigate("Profile");
                }}
                style={styles.s7fd2bed2}
              >
                <Text style={styles.s56e8ed08}>Continue</Text>
              </TouchableHighlight>
            </View>
        </View>
      </SafeAreaView>
    </Fragment>
  );
};

_08.defaultProps = {};

export default _08;

export { imgScreenShot20190917At164453, styles };
