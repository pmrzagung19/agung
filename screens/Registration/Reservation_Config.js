import React, { Fragment } from "react";
import {
  Text,
  View,
  StyleSheet
} from "react-native";
import { Container, Content, Item, Label, Icon, Button, Row } from "native-base";
import { Switch } from 'react-native-paper';
import InputSpinner from "react-native-input-spinner";

class Reservation_Config extends React.Component{
    constructor(props) {
        console.log('value')
        super(props)
        this.state = {
          value: 0,
          value1: 0,
          value2: 0,
          amount:0
        }
        this.amount = 0
      }
      changeAmount(text) {
        this.amount = text
      }

      state = {
        isSwitchOn: false,
      };
  
  render(){
    const { isSwitchOn } = this.state;
    const{navigate} = this.props.navigation;
    return (
      <Container>
          <Content contentContainerStyle={styles.contentContainer}>
            <Row style={styles.Row1}>
                <Text style={styles.Text1}>
                Define <Text style={styles.TextInText}>reservation configuration </Text>you would have
                </Text>
                <Text style={styles.Text2}>3/4</Text>
            </Row>
            <Content contentContainerStyle={styles.Content1}>
                <Text style={styles.Text3}>Max number of serving capacity (in person)</Text>
                <Content contentContainerStyle={styles.Content2}>
                    <Row >
                        <Row style={styles.Row2}>
                            <Icon name='user' type='FontAwesome5' style={styles.Icon}/>
                            <Item floatingLabel style={styles.floatingLabel}>
                                <Label>Senior</Label>
                            </Item>
                        </Row>
                        <Row style={styles.Row2}>
                            <Icon name='user' type='FontAwesome5' style={styles.Icon}/>
                            <Item floatingLabel style={styles.floatingLabel}>
                                <Label>Adult</Label>
                            </Item>
                        </Row>
                        <Row style={styles.Row2}>
                            <Icon name='user' type='FontAwesome5' style={styles.Icon1}/>
                            <Item floatingLabel style={styles.floatingLabel}>
                                <Label>Kid</Label>
                            </Item>
                        </Row>
                    </Row>
                </Content>
                <Text style={styles.TextSpinner}>Max number of serving capacity (in person)</Text>               
                <Content contentContainerStyle={styles.ContentSpinner}>
                  <InputSpinner
                    value={this.state.value}
                    style={styles.spinner}
                    rounded={false}
                    color={'#009371'}
                  />
                </Content>
                <Text style={styles.TextSpinner}>Waiting time estimation (in minute)</Text>
                <Content contentContainerStyle={styles.ContentSpinner}>
                  <InputSpinner
                    value={this.state.value}
                    style={styles.spinner}
                    rounded={false}
                    color={'#009371'}
                  />
                </Content>
                <Content contentContainerStyle={styles.Content3}>
                    <Row style={styles.Row3}>
                        <Text style={styles.Text4}>Anti-broker system</Text>
                        <Switch
                        value={isSwitchOn}
                        onValueChange={() =>
                        { this.setState({ isSwitchOn: !isSwitchOn }); }
                        }
                        />
                    </Row>
                </Content>
            </Content>
            <View style={styles.ButtonView}>
              <Button onPress={()=>navigate('Guest_Reminder')} style={styles.Button}>
                  <Text style={styles.ButtonText}>Continue</Text>
              </Button>
            </View>
          </Content>
      </Container>
   );
  }
}

export default Reservation_Config;

const styles = StyleSheet.create({
  spinner: {
    width: "auto",
    minWidth:300,
    borderBottomColor:'green',
    borderBottomWidth:1
  },
  contentContainer: {
    flex:1,
    paddingHorizontal:20,
    paddingVertical:20
  },
  Row1: {
    height:80,
    paddingRight:15
  },
  Text1: {
    fontSize:25,
    fontWeight:'100',
    marginHorizontal:10,
    paddingBottom:10
  },
  TextInText: {
    fontWeight:'bold'
  },
  Text2: {
    color:'grey',
    fontSize:15,
    paddingTop:15
  },
  Content1: {
    alignContent:'space-between'
  },
  Text3: {
    color:'grey',
    fontSize:16,
    paddingLeft:13,
    marginBottom:10
  },
  Content2: {
    height:60,
    paddingBottom:10
  },
  Row2: {
    justifyContent:'center'
  },
  Icon: {
    marginTop:20,
    paddingRight:5,
    fontSize:23
  },
  Icon1: {
    marginTop:25,
    paddingRight:5,
    fontSize:18
  },
  TextSpinner: {
    color:'grey',
    fontSize:16,
    paddingLeft:13,
    marginTop:15,
    marginBottom:5
  },
  ContentSpinner: {
    flex:1,
    alignItems:'center'
  },
  Content3: {
    paddingVertical:10
  },
  Row3: {
    flex:1,
    justifyContent:'space-between'
  },
  Text4: {
    color:'grey',
    fontSize:16,
    paddingLeft:10
  },
  ButtonView: {
    flex:0.25,
    justifyContent:'flex-start'
  },
  Button: {
    borderRadius:20,
    backgroundColor:'#009371',
    justifyContent:'center'
  },
  ButtonText: {
    fontSize:18,
    fontWeight:'bold',
    color:'white'
  },
  floatingLabel: {
    width:60
  },
});

export { styles };
