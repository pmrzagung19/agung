import React, { Fragment } from "react";
import {
  Text,
  View,
  StyleSheet
} from "react-native";
import { Container, Content, Button, Row } from "native-base";
import InputSpinner from "react-native-input-spinner";
import { Switch } from 'react-native-paper';

class Guest_Reminder extends React.Component{
    constructor(props) {
        console.log('value')
        super(props)
        this.state = {
          value: 0,
          value1: 0,
          value2: 0,
          amount:0
        }
        this.amount = 0
      }
      changeAmount(text) {
        this.amount = text
      }

      state = {
        isSwitchOn: false,
      };
  
  render(){
    const { isSwitchOn } = this.state;
    const{navigate} = this.props.navigation;
    return (
      <Container>
          <Content contentContainerStyle={styles.Content1}>
            <Row style={styles.Row1}>
                <Text style={styles.Text1}>
                Define <Text style={styles.TextInText1}>guest reminder </Text>you would have
                </Text>
                <Text style={styles.Text2}>4/4</Text>
            </Row>
            <Content contentContainerStyle={styles.Content2}>
                <Text style={styles.Text3}>Number of notification(s)</Text>               
                <Content contentContainerStyle={styles.Content3}>
                  <InputSpinner
                    value={this.state.value}
                    style={styles.spinner}
                    rounded={false}
                    color={'#009371'}
                  />
                </Content>
                <Text style={styles.Text4}>Notification time interval (in second)</Text>
                <Content contentContainerStyle={styles.Content4}>
                  <InputSpinner
                    value={this.state.value}
                    style={styles.spinner}
                    rounded={false}
                    color={'#009371'}
                  />
                </Content>
                <Content contentContainerStyle={styles.Content5}>
                    <Row style={styles.Row2}>
                        <Text style={styles.Text5}>Retake queue number</Text>
                        <Switch
                        value={isSwitchOn}
                        color={'#009371'}
                        onValueChange={() =>
                        { this.setState({ isSwitchOn: !isSwitchOn }); }
                        }
                        />
                    </Row>
                </Content>
            </Content>
            <View style={styles.ButtonView}>
              <Button onPress={()=>navigate('Reservation_Daftar')} style={styles.Button}>
                  <Text style={styles.ButtonText}>Continue</Text>
              </Button>
            </View>
          </Content>
      </Container>
   );
  }
}

export default Guest_Reminder;

const styles = StyleSheet.create({
  spinner: {
    width: "auto",
    minWidth:300,
    borderBottomColor:'green',
    borderBottomWidth:1
  },
  Content1: {
    flex:1,
    paddingHorizontal:20,
    paddingVertical:20
  },
  Row1: {
    height:130,
    paddingRight:15
  },
  Text1: {
    fontSize:30,
    fontWeight:'100',
    marginHorizontal:10,
    paddingBottom:20
  },
  TextInText1: {
    fontWeight:'bold'
  },
  Text2: {
    color:'grey',
    fontSize:15,
    paddingTop:15
  },
  Content2: {
    alignContent:'space-between'
  },
  Text3: {
    color:'grey',
    fontSize:16,
    paddingLeft:13,
    marginBottom:5
  },
  Content3: {
    flex:1,
    alignItems:'center'
  },
  Text4: {
    color:'grey',
    fontSize:16,
    paddingLeft:13,
    marginTop:20,
    marginBottom:5
  },
  Content4: {
    flex:1,
    alignItems:'center'
  },
  Content5: {
    paddingVertical:10
  },
  Row2: {
    flex:1,
    justifyContent:'space-between'
  },
  Text5: {
    color:'grey',
    fontSize:16,
    paddingLeft:10
  },
  ButtonView: {
    flex:0.25,
    justifyContent:'flex-start'
  },
  Button: {
    borderRadius:20,
    backgroundColor:'#009371',
    justifyContent:'center'
  },
  ButtonText: {
    fontSize:18,
    fontWeight:'bold',
    color:'white'
  },
});

export { styles };