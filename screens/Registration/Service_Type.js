import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import { Content, Container,Text, Icon, Radio, ListItem, Left, Right, Button, View, Form, Item, Label, Row } from 'native-base';
import Modal from "react-native-modal";
import { RadioButton } from 'react-native-paper';


class Service_Type extends Component {
  state = {
    isModalVisible: true,
    checked:'zero'
  };

  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  };

  render() {
    const { checked } = this.state;
    const{navigate} = this.props.navigation;
    return (
      <Container >
        <Content contentContainerStyle={{flex:1}}>
          <Modal
            isVisible={this.state.isModalVisible} 
            animationType={"slide"}
            backdropOpacity={0.8}
            deviceHeight={null}
            deviceWidth={null}
            style={{
            }}>
              <View style={{backgroundColor:'white',padding:20,borderRadius:20,alignItems:'center',justifyContent:'center'}}>
                <Icon name='check-circle' type='MaterialCommunityIcons' style={{color:'green',fontSize:80}}/>
                <Text style={{marginVertical:20,fontWeight:'bold',fontSize:30}}>Success</Text>
                <Text>You are now registered as a member of</Text>
                <Text style={{fontWeight:'bold'}}>Sushi Tei Indonesia</Text>
                <Text onPress={this.toggleModal} style={{marginTop:25,fontWeight:'bold',fontSize:20}}>OK</Text>
              </View>
          </Modal>
          <Content style={{flex:2}}>
            <Row style={{paddingRight:15}}>
                <Text style={{fontSize:25,paddingTop:22,paddingHorizontal:20}}>First of all, set your</Text>
                <Text style={{flex:1,color:'grey',fontSize:15,alignSelf:'flex-end',textAlign:'right',paddingRight:20}}>1/4</Text>
            </Row>
            <Text style={{fontSize:25,fontWeight:'bold',marginLeft:20}}>service type</Text>
            <Text style={{paddingTop:15,marginBottom:-20,color:'grey',marginLeft:25}}>Service type</Text>
            <ListItem style={styles.ListItem}>
              <Left style={{paddingLeft:15}}>
                <RadioButton
                  value="first"
                  status={checked === 'first' ? 'checked' : 'unchecked'}
                  onPress={() => { this.setState({ checked: 'first' }); }}
                />
                <Text style={styles.TextRadio}>Reservation</Text>
              </Left>
            </ListItem>
            <ListItem style={styles.ListItem}>
              <Left style={{marginLeft:15}}>
                <RadioButton
                  value="second"
                  status={checked === 'second' ? 'checked' : 'unchecked'}
                  onPress={() => { this.setState({ checked: 'second' }); }}
                />
                <Text style={styles.TextRadio}>Customer Service</Text>
              </Left>
            </ListItem>
            <ListItem style={styles.ListItem}>
              <Left style={{marginLeft:15}}>
                <RadioButton
                  value="third"
                  status={checked === 'third' ? 'checked' : 'unchecked'}
                  onPress={() => { this.setState({ checked: 'third' }); }}
                />
                <Text style={styles.TextRadio}>Payment</Text>
              </Left>
            </ListItem>
            <ListItem style={styles.ListItem}>
              <Left style={{marginLeft:15}}>
                <RadioButton
                  value="fourth"
                  status={checked === 'fourth' ? 'checked' : 'unchecked'}
                  onPress={() => { this.setState({ checked: 'fourth' }); }}
                />
                <Text style={styles.TextRadio}>Taking Order</Text>
              </Left>
            </ListItem>
            <ListItem style={styles.ListItem5}>
              <Left style={{marginLeft:15}}>
                <RadioButton
                  value="fifth"
                  status={checked === 'fifth' ? 'checked' : 'unchecked'}
                  onPress={() => { this.setState({ checked: 'fifth' }); }}
                />
                <Text style={styles.TextRadio}>Others</Text>
              </Left>
            </ListItem>
          </Content>
          <View style={styles.ViewItem}>
              <Item floatingLabel style={styles.Item}>
                <Label style={styles.TextRadio}>Name your service</Label>
              </Item>
          </View>
          <View style={styles.ViewButton}>
          <Button onPress={()=>navigate('Reservation')} style={styles.Button}>
                  <Text style={styles.TextButton}>Continue</Text>
              </Button>
          </View>
        </Content>
      </Container>
    );
  }
}

export default Service_Type;

export { styles };

const styles = StyleSheet.create({
  ViewButton: {
    flex:0.4,
    justifyContent:'center',
    padding:20
  },
  Button: {
    borderRadius:20,
    backgroundColor:'#009371',
    justifyContent:'center'
  },
  TextButton: {
    fontSize:18,
    fontWeight:'bold',
    color:'white'
  },
  ViewItem: {
    flex:0.5,
    paddingHorizontal:25
  },
  Item: {
    marginVertical:20
  },
  TextRadio: {
    fontSize:18
  },
  ListItem: {
    borderBottomWidth:0,
    marginBottom:-35,
    marginLeft:0
  },
  ListItem5: {
    borderBottomWidth:0,
    marginBottom:0,
    marginLeft:0
  },
});