import React, { Component } from 'react';
import { View, Text, FlatList, ActivityIndicator } from 'react-native';
import { ListItem, SearchBar } from 'react-native-elements';
import { Content, Item, Icon, Input, Button, Container } from 'native-base';

class Search_Brand extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      data: [],
      error: null,
    };

    this.arrayholder = [];
  }

  componentDidMount() {
    this.makeRemoteRequest();
  }

  makeRemoteRequest = () => {
    const url = `https://randomuser.me/api/?&results=20`;
    this.setState({ loading: true });

    fetch(url)
      .then(res => res.json())
      .then(res => {
        this.setState({
          data: res.results,
          error: res.error || null,
          loading: false,
        });
        this.arrayholder = res.results;
      })
      .catch(error => {
        this.setState({ error, loading: false });
      });
  };

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: '86%',
          backgroundColor: '#CED0CE',
          marginLeft: '14%',
        }}
      />
    );
  };

  searchFilterFunction = text => {
    this.setState({
      value: text,
    });

    const newData = this.arrayholder.filter(item => {
      const itemData = `${item.name.title.toUpperCase()} ${item.name.first.toUpperCase()} ${item.name.last.toUpperCase()}`;
      const textData = text.toUpperCase();

      return itemData.indexOf(textData) > -1;
    });
    this.setState({
      data: newData,
    });
  };


  render() {
    const{navigate}=this.props.navigation;
    return (
      <Container>
        <View style={{flex:1}}>
          <View style={{flex:3.3,padding:20}}>
          <Text style={{fontSize:25,fontWeight:'100',marginHorizontal:10,marginBottom:20}}>
            Please select brand that are the <Text style={{fontWeight:'bold'}}>parent </Text>of your account
          </Text>
            <Item rounded style={{marginRight:15,paddingHorizontal:10}}>
              <Input 
                  placeholder='Search'
                  onChangeText={text => this.searchFilterFunction(text)}
                  autoCorrect={false}
                  value={this.state.value}
                />
              <Icon name='search'/>
            </Item>
            <FlatList
              style={{paddingBottom:20}}
              data={this.state.data}
              renderItem={({ item }) => (
                <ListItem
                  leftAvatar={{ source: { uri: item.picture.thumbnail } }}
                  title={`${item.name.first} ${item.name.last}`}
                  subtitle={item.email}
                />
              )}
              keyExtractor={item => item.email}
            />
            <Button onPress={()=>navigate('Pin_Enter')} style={{borderRadius:20,backgroundColor:'#009371',justifyContent:'center'}}>
              <Text style={{fontSize:18,fontWeight:'bold',color:'white'}}>Continue</Text>
            </Button>
          </View>
      </View>
    </Container>
    );
  }
}

export default Search_Brand;