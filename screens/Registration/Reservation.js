import React, { Fragment } from "react";
import {
  Text,
  View,
  StyleSheet
} from "react-native";
import { SafeAreaView } from "react-navigation";
import { Container, Content, Button, Row } from "native-base";
import InputSpinner from "react-native-input-spinner";

class Reservation extends React.Component{
    constructor(props) {
        console.log('value')
        super(props)
        this.state = {
          value: 0,
          value1: 0,
          value2: 0,
          amount:0
        }
        this.amount = 0
      }
      changeAmount(text) {
        this.amount = text
      }
  
  render(){
    const{navigate} = this.props.navigation;
    return (
      <Container>
          <Content contentContainerStyle={styles.MainContent}>
            <Row style={styles.Row}>
                <Text style={styles.MainText}>
                  How many <Text style={styles.TextBold}>counter </Text>would you have?
                </Text>
                <Text style={styles.TextNumber}>2/4</Text>
            </Row>
            <Content contentContainerStyle={styles.ContentSpinner}>
              <InputSpinner
                value={this.state.value}
                style={styles.spinner}
                rounded={false}
                color={'#009371'}
              />
              <InputSpinner
                value={this.state.value}
                style={styles.spinner}
                rounded={false}
                color={'#009371'}
              />
              <InputSpinner
                value={this.state.value}
                style={styles.spinner}
                rounded={false}
                color={'#009371'}
              />
            </Content>
            <View style={styles.ViewButton}>
              <Button onPress={()=>navigate('Reservation_Config')} style={styles.Button}>
                  <Text style={styles.TextButton}>Continue</Text>
              </Button>
            </View>
          </Content>
      </Container>
   );
  }
}

export default Reservation;

export { styles };

const styles = StyleSheet.create({
  MainContent: {
    flex:1,
    paddingHorizontal:20,
    paddingVertical:20
  },
  ViewButton: {
    flex:0.2,
    justifyContent:'center',
    marginVertical:20
  },
  TextNumber: {
    color:'grey',
    fontSize:15,
    paddingTop:15
  },
  MainText: {
    fontSize:30,
    fontWeight:'100',
    marginHorizontal:10,
    paddingBottom:20
  },
  Row: {
    height:90,
    paddingRight:15
  },
  TextBold: {
    fontWeight:'bold'
  },
  Button: {
    borderRadius:20,
    backgroundColor:'#009371',
    justifyContent:'center'
  },
  TextButton: {
    fontSize:18,
    fontWeight:'bold',
    color:'white'
  },
  spinner: {
    width: "auto",
    minWidth:300,
    borderBottomColor:'#009371',
    borderBottomWidth:1
  },
  ContentSpinner: {
    flex:1,
    alignItems:'center',
    justifyContent:'space-between'
  },
});
