import React, { Fragment } from "react";
import {
  Text,
  View,
  StyleSheet
} from "react-native";
import { SafeAreaView } from "react-navigation";
import { Container, Button, Thumbnail } from "native-base";

class Next_Regis extends React.Component{
  
  state={
    icon:"eye-off",
    icon2:"eye-off",
    type:"Entypo",
    password:true,
    password2:true,
    text:''
  };

  _changeIcon(){
    this.setState(prevState => ({
        icon: prevState.icon === 'eye' ? 'eye-off' : 'eye',
        password: !prevState.password
    }));
  }
  _changeIcon2(){
    this.setState(prevState => ({
        icon2: prevState.icon2 === 'eye' ? 'eye-off' : 'eye',
        password2: !prevState.password2
    }));
  }
  
  render(){
    const{icon} = this.props;
    const{navigate} = this.props.navigation;
    return (
        <Container>
            <View style={styles.View}>
                <View style={styles.ViewIn}>
                    <Thumbnail
                        source={require('./bide-empty.png')}
                        style={styles.Thumbnail}
                    />
                    <Text style={styles.Text1}>Thank you for using Bide!</Text>
                    <Text style={styles.Text2}>You will be required to ﬁll out your business proﬁle before you can use the full features of this queueing system </Text>
                </View>
                <View style={styles.View2}>
                    <Button onPress={()=>navigate('Form_Profile')} style={styles.Button}>
                        <Text style={styles.TextButton}>Continue</Text>
                    </Button>
                </View>
            </View>
        </Container>
   );
  }
}

export default Next_Regis;

const styles = StyleSheet.create({
  View: {
    flex:1
  },
  ViewIn: {
    flex:3.3,
    alignItems:'center',
    justifyContent:'center'
  },
  Thumbnail: {
    height:200,
    width:200
  },
  Text1: {
    fontWeight:'bold',
    fontSize:20,
    paddingVertical:10
  },
  Text2: {
    fontSize:15,
    paddingHorizontal:35,
    textAlign:'center'
  },
  View2: {
    flex:1,
    justifyContent:'center',
    paddingHorizontal:20
  },
  Button: {
    borderRadius:20,
    backgroundColor:'#009371',
    justifyContent:'center'
  },
  TextButton: {
    fontSize:18,
    fontWeight:'bold',
    color:'white'
  },
});

export { styles };
