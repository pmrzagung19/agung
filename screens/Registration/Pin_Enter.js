import React, { Fragment } from "react";
import { Text, View, StyleSheet } from "react-native";
import { SafeAreaView } from "react-navigation";
import SmoothPinCodeInput from 'react-native-smooth-pincode-input';
import { Content, Button } from "native-base";

class Pin_Enter extends React.Component{
  state = {
    code: '',
    password: '',
  };
  pinInput = React.createRef();

  _checkCode = (code) => {
    if (code != '123456') {
      this.pinInput.current.shake()
        .then(() => this.setState({ code: '' }));
    }
  }

render(){
  const { code, password } = this.state;
  const {navigate} = this.props.navigation
  return (
    <Fragment>
      <SafeAreaView style={styles.SafeAreaView}>
        <View style={styles.View}>
          <Text style={styles.Text}>
            Please enter <Text style={{fontWeight: 'bold'}}>Sushi Tei code</Text> to verify
          </Text>
          <View style={styles.ViewIn}>
          <SmoothPinCodeInput
              ref={this.pinInput}
              value={code}
              codeLength={6}
              onTextChange={code => this.setState({ code })}
              onFulfill={this._checkCode}
              onBackspace={this._focusePrevInput}
              cellStyle={{
                borderRadius: 4,
                borderWidth: 2,
                borderColor: 'rgba(0, 145, 95, 1)',
              }}
              // cellSpacing={10}
              cellSize={50}
              cellStyleFocused={{
                borderColor: 'rgba(0, 145, 95, 1)',
              }}
              />
          </View>
        </View>
        <Content contentContainerStyle={styles.Content}>
          <Button onPress={()=>navigate('Service_Type')} style={styles.Button}>
            <Text style={styles.TextButton}>Continue</Text>
          </Button>
        </Content>
      </SafeAreaView>
    </Fragment>
  );
};
}

const styles = StyleSheet.create({
  Text: {
    fontSize: 28,
    padding:10,
    fontWeight: `500`
  },
  Content: {
    flex:1,
    justifyContent:'flex-end',
    marginBottom:50,
    marginHorizontal:20
  },
  Button: {
    borderRadius:20,
    backgroundColor:'#009371',
    justifyContent:'center'
  },
  TextButton: {
    fontSize:18,
    fontWeight:'bold',
    color:'white'
  },
  ViewIn: {
    flexDirection: `row`,
    justifyContent: `center`,
    padding: 20
  },
  View: {
    flex: 2,
    padding: 20
  },
  SafeAreaView: {
    flex: 1,
    alignItems: `stretch`,
    flexDirection: `column`
  }
});

export default Pin_Enter;