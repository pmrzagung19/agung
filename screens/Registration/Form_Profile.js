import React, { Fragment } from "react";
import {
  Text,
  View,
  StyleSheet
} from "react-native";
import { SafeAreaView } from "react-navigation";
import { Container, Content, Form, Item, Label, Button } from "native-base";
import RNPickerSelect from 'react-native-picker-select';

class Form_Profile extends React.Component{
  
  constructor(props) {
    super(props);

    this.inputRefs = {
      firstTextInput: null,
      favSport0: null,
      favSport1: null,
      lastTextInput: null,
      favSport5: null,
    };

    this.state = {
      numbers: [
        {
          label: '1',
          value: 1,
          color: 'orange',
        },
        {
          label: '2',
          value: 2,
          color: 'green',
        },
      ],
      favSport0: undefined,
      favSport1: undefined,
      favSport2: undefined,
      favSport3: undefined,
      favSport4: 'baseball',
      previousFavSport5: undefined,
      favSport5: null,
      favNumber: undefined,
    };
  }
  render(){
    const{icon} = this.props;
    const{navigate} = this.props.navigation;
    return (
        <Container>
            <View style={styles.ViewContainer}>
              <View style={styles.View}>  
                <Text style={styles.TextView}>
                    Provide your <Text style={styles.TextInText}>profile </Text>details
                </Text>
                <Content showsVerticalScrollIndicator={false}>
                    <Form>
                        <Item floatingLabel>
                            <Label style={styles.FloatingLabel}>Store / outlet / branch name</Label>
                        </Item>
                        <Item floatingLabel>
                            <Label style={styles.FloatingLabel}>Industry</Label>
                        </Item>
                        <Item floatingLabel>
                            <Label style={styles.FloatingLabel}>Phone number</Label>
                        </Item>
                        <Item floatingLabel>
                            <Label style={styles.FloatingLabel}>Area name</Label>
                        </Item>
                        <Content contentContainerStyle={styles.Content1}>
                        <RNPickerSelect
                              placeholder={{label:'Country',value:null,color: '#9EA0A4'}}
                              style={pickerSelectStyles}
                              onValueChange={(value) => console.log(value)}
                              items={[
                                  { label: 'Indonesia', value: 'Indonesia' },
                                  { label: 'Singapura', value: 'Singapura' },
                                  { label: 'Australia', value: 'Australia' },
                              ]}
                          />
                        </Content>
                        <Content contentContainerStyle={styles.Content1}>
                        <RNPickerSelect
                              placeholder={{label:'Province',value:null,color: '#9EA0A4'}}
                              style={pickerSelectStyles}
                              onValueChange={(value) => console.log(value)}
                              items={[
                                  { label: 'Jawa', value: 'Jawa' },
                                  { label: 'Kalimantan', value: 'Kalimantan' },
                                  { label: 'Sumatera', value: 'Sumatera' },
                              ]}
                          />
                        </Content>
                        <Content contentContainerStyle={styles.Content1}>
                          <RNPickerSelect
                              placeholder={{label:'City',value:null,color: '#9EA0A4'}}
                              style={pickerSelectStyles}
                              onValueChange={(value) => console.log(value)}
                              items={[
                                  { label: 'Tangerang', value: 'Tangerang' },
                                  { label: 'Surabaya', value: 'Surabaya' },
                                  { label: 'Bandung', value: 'Bandung' },
                              ]}
                          />
                        </Content>
                        <Item floatingLabel>
                            <Label style={styles.FloatingLabel}>Street address</Label>
                        </Item>
                        <Item floatingLabel>
                            <Label style={styles.FloatingLabel}>Postal code</Label>
                        </Item>
                    </Form>
                </Content>
                </View>
                <View style={styles.ButtonView}>
                    <Button onPress={()=>navigate('Business_Profile')} style={styles.Button}>
                        <Text style={styles.ButtonText}>Continue</Text>
                    </Button>
                </View>
            </View>
        </Container>
   );
  }
}

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 16,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: 'purple',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
});

const styles = StyleSheet.create({
  FloatingLabel: {
    fontSize:15
  },
  ButtonView: {
    flex:1,
    justifyContent:'center',
    paddingHorizontal:20
  },
  Button: {
    borderRadius:20,
    backgroundColor:'#009371',
    justifyContent:'center'
  },
  ButtonText: {
    fontSize:18,
    fontWeight:'bold',
    color:'white'
  },
  ViewContainer: {
    flex:1
  },
  View: {
    flex:3.3,
    padding:20
  },
  TextView: {
    fontSize:25,
    fontWeight:'100',
    marginHorizontal:10,
    paddingBottom:15
  },
  TextInText: {
    fontWeight:'bold'
  },
  Content1: {
    marginLeft:10,
    marginTop:10,
    borderColor:'#9EA0A4',
    borderBottomWidth:1
  },
  Content2: {
    marginLeft:10,
    marginVertical:10,
    borderColor:'#9EA0A4',
    borderBottomWidth:1
  },
  
});

export default Form_Profile;

export { styles };
