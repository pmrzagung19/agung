import React, { Fragment } from "react";
import {
  Text,
  View,
  StyleSheet
} from "react-native";
import { SafeAreaView } from "react-navigation";
import { Container, Button } from "native-base";

class Business_Profile extends React.Component{

  render(){
    const{navigate} = this.props.navigation;
    return (
        <Container>
            <View style={styles.ContainerView}>
                <View style={styles.InnerView}>
                    <Text style={styles.TextView}>
                    Has your business profile <Text style={styles.TextInText}>previously been registered </Text>
                    in Bide! application?
                    </Text>
                </View>
                <View style={styles.ButtonView}>
                    <Button onPress={()=>navigate('Search_Brand')} style={styles.Button}>
                        <Text style={styles.ButtonText}>No, I have not. This is the first time</Text>
                    </Button>
                </View>
                <View style={styles.ButtonView}>
                    <Button transparent onPress={()=>navigate('Reservation_Daftar')} style={styles.Button1}>
                        <Text style={styles.ButtonText1}>Yes, I have</Text>
                    </Button>
                </View>
            </View>
        </Container>
   );
  }
}

export default Business_Profile;

const styles = StyleSheet.create({
  ContainerView: {
    flex:1
  },
  InnerView: {
    padding:20
  },
  TextView: {
    fontSize:24,
    marginHorizontal:5
  },
  TextInText: {
    fontWeight:'bold'
  },
  ButtonView: {
    paddingHorizontal:20,
    paddingTop:15
  },
  Button: {
    borderRadius:20,
    backgroundColor:'#009371',
    justifyContent:'center'
  },
  ButtonText: {
    fontSize:18,
    fontWeight:'bold',
    color:'white'
  },
  Button1: {
    borderColor:'#009371',
    borderWidth:2.4,
    borderRadius:20,
    justifyContent:'center'
  },
  ButtonText1: {
    fontSize:18,
    fontWeight:'bold',
    color:'#009371'
  },  
});

export { styles };
