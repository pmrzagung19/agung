import React, { Fragment } from "react";
import { Text, StyleSheet } from "react-native";
import { SafeAreaView } from "react-navigation";
import SmoothPinCodeInput from 'react-native-smooth-pincode-input';
import { Content, Button, View, Container } from "native-base";

class Pin_Regis extends React.Component{
  state = {
    code: '',
  };
  pinInput = React.createRef();

  _checkCode = (code) => {
    const { navigate } = this.props.navigation;

    if (code != '123456') {
      this.pinInput.current.shake()
        .then(() => this.setState({ code: '' }))
    } else {
      navigate('Welcome')
    }
  }
render(){
  const { code, password } = this.state;
  const {navigate} = this.props.navigation
  return (
    <Container>
      {/* <SafeAreaView style={styles.SafeAreaView}> */}
        <View style={styles.ViewIn}>
          <Text style={styles.Text}>
            Enter <Text style={styles.TextInText}>verification code</Text> we've just send you.
          </Text>
          <View style={styles.ViewInView}>
          <SmoothPinCodeInput
              codeLength={6}
              ref={this.pinInput}
              value={code}
              onTextChange={code => this.setState({ code })}
              onFulfill={this._checkCode}
              onBackspace={this._focusePrevInput}
              cellStyle={{
                borderRadius: 4,
                borderWidth: 2,
                borderColor: 'rgba(0, 145, 95, 1)',
              }}
              // cellSpacing={10}
              cellSize={50}
              cellStyleFocused={{
                borderColor: 'rgba(0, 145, 95, 1)',
              }}
              />
          </View>
          <Text style={styles.TextCountdown}>Time remaining 01:30</Text>
          <Text style={styles.TextResend}>
            Resend
          </Text>
        </View>
        <Content contentContainerStyle={styles.Content}>
          <Button onPress={()=>navigate('Next_Regis')} style={styles.Button}>
            <Text style={styles.TextButton}>Continue</Text>
          </Button>
        </Content>
      {/* </SafeAreaView> */}
    </Container>
  );
};
}

const styles = StyleSheet.create({
  Text: {
    fontSize: 23,
    fontWeight: `500`
  },
  Button: {
    borderRadius:20,
    backgroundColor:'#009371',
    justifyContent:'center'
  },
  TextButton: {
    fontSize:18,
    fontWeight:'bold',
    color:'white'
  },
  Content: {
    flex:1,
    justifyContent:'flex-end',
    marginBottom:50,
    marginHorizontal:20
  },
  TextInText: {
    fontWeight: 'bold'
  },
  ViewInView: {
    flexDirection: `row`,
    justifyContent: `center`,
    padding: 20
  },
  TextCountdown: {
    textAlign: `center`
  },
  TextResend: {
    textDecorationLine: `underline`,
    color: `rgba(0, 145, 95, 1)`,
    fontWeight: `500`,
    textAlign: `center`
  },
  ViewIn: {
    flex: 2,
    padding: 20
  },
  SafeAreaView: {
    flex: 1,
    alignItems: `stretch`,
    flexDirection: `column`
  }
});

export default Pin_Regis;