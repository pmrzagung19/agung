import React, { Fragment } from "react";
import {
  Text,
  StyleSheet,
  Keyboard
} from "react-native";
import { SafeAreaView, StackActions, NavigationActions } from "react-navigation";
import { connect } from 'react-redux';
import {
  regisUser
} from '../store/actions';
import Spinner from '../common/spinner';
import auth from '@react-native-firebase/auth';
import { Container, Content, Input, Form, Item, Label, Icon, Button, View } from "native-base";

class Registration extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      email:'',
      password:'',
      cpassword:"",
      icon:"eye-off",
      icon2:"eye-off",
      type:"Entypo",
      passwordHide1:true,
      passwordHide2:true,
      errorMessage:null
    };
  }

  _changeIcon(){
    this.setState(prevState => ({
        icon: prevState.icon === 'eye' ? 'eye-off' : 'eye',
        passwordHide1: !prevState.passwordHide1
    }));
  }

  _changeIcon2(){
    this.setState(prevState => ({
        icon2: prevState.icon2 === 'eye' ? 'eye-off' : 'eye',
        passwordHide2: !prevState.passwordHide2
    }));
  }

  Validation = () => {
    const { email, password, cpassword } = this.state;
    if(email == ''){
      alert('Fill Email')
      return false
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email)){
      alert('Email Invalid')
      return false
    } else if (password == ''){
      alert('Fill Password')
      return false
    } else if (password.length < 6){
      alert('Minimum Password length 6')
      return false
    } else if (cpassword == ''){
      alert('Fill Confirm Password')
      return false
    } else if (cpassword !== password){
      alert('Password not same')
      return false
    }
    return true
  }

  errorRender(){
    if(this.props.err_regis){
      return(
        <View style={{backgroundColor:'white'}}>
          <Text style={{color:'red', fontSize:18,textAlign:'center'}}>
            {this.props.err_regis}
          </Text>
        </View>
      );
    }
  }

  onButtonPress(){
    const { email, password } = this.state;

    if(this.Validation()){
      this.props.regisUser(email, password)
      Keyboard.dismiss()
    }
  }

  renderButton(){
    if(this.props.loading){
      return(
        <Spinner/>
      )
    }
    return(
      <Button onPress={this.onButtonPress.bind(this)} style={styles.button}>
        <Text style={styles.text2}>Continue</Text>
      </Button>
    );
  }

  componentDidMount(){
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'Pin_Regis' })],
    });

    const { navigate, dispatch } = this.props.navigation;

    auth().onAuthStateChanged(function(user) {
      if (user) {
        console.log('User has Logged')
        navigate('Pin_Regis')
      }
    });
    
  }
  
  render(){
    const{icon} = this.props;
    return (
      <Container>
          <Content contentContainerStyle={styles.content}>
          <Content style={{flex:1,marginVertical:20}}>
            <Text style={styles.text}>
              Welcome! Create your <Text style={styles.text1}>account </Text>now
            </Text>
            {this.errorRender()}
              <Form>
                <Item floatingLabel>
                  <Label>Email</Label>
                  <Input
                  ref={(textInput) => this._email = textInput }
                  style={styles.inputField}
                  value={this.state.text}
                  onChangeText={(email) => this.setState({email})}
                  editable={true}
                  maxLength={40}
                  multiline={false}
                  autoCapitalize="none"
                  autoCorrect={false}
                  keyboardType='email-address'
                  />
                </Item>
                <Item floatingLabel>
                  <Icon active name={icon}/>
                  <Label>Password</Label>
                  <Input 
                    ref={(textInput) => this._password = textInput }
                    style={styles.inputField}
                    value={this.state.text}
                    onChangeText={(password)=> this.setState({password})}
                    editable={true}
                    maxLength={40}
                    multiline={false}
                    secureTextEntry={this.state.passwordHide1}
                    autoCapitalize="none"
                    autoCorrect={false}
                  />
                  <Icon 
                    name={this.state.icon} onPress={() => this._changeIcon()}
                  />
                </Item>
                <Item floatingLabel>
                  <Icon active name={icon}/>
                  <Label>Confirm password</Label>
                  <Input
                    ref={(textInput) => this.c_password = textInput }
                    style={styles.inputField}
                    value={this.state.text}
                    onChangeText={(cpassword)=> this.setState({cpassword})}
                    editable={true}
                    maxLength={40}
                    multiline={false}
                    secureTextEntry={this.state.passwordHide2}
                    autoCapitalize="none"
                    autoCorrect={false}
                  />
                  <Icon 
                    name={this.state.icon2} onPress={() => this._changeIcon2()}
                  />
                </Item>
              </Form>
            </Content>
            <View style={styles.View1}>
              {this.renderButton()}
            </View>
          </Content>
      </Container>
   );
  }
}

const styles = StyleSheet.create({
  content: {
    flex:1,
    paddingHorizontal:25,
    paddingTop: 10,
    paddingBottom:30
  },
  text: {
    fontSize:30,
    fontWeight:'100',
    marginHorizontal:10
  },
  text1: {
    fontWeight:'bold'
  },
  View1: {
    flex:0.1,
    justifyContent:'center',
    marginVertical:20
  },
  content1: {
    flex:1,
    justifyContent:'flex-end',
    marginVertical:20
  },
  button: {
    borderRadius:20,
    backgroundColor:'#009371',
    justifyContent:'center'
  },
  text2: {
    fontSize:18,
    fontWeight:'bold',
    color:'white'
  },
});

export { styles };

const mapStateToProps = ({ auth }) => {
  const { err_regis, loading } = auth;

  return { err_regis, loading };
};

export default connect(mapStateToProps, { 
  regisUser
}) (Registration);