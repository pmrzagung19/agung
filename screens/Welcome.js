import React, { Fragment } from "react";
import {
  Text,
  StyleSheet
} from "react-native";
import { SafeAreaView, StackActions, NavigationActions } from "react-navigation";
import LinearGradient from 'react-native-linear-gradient';
import { Container, Content, Col, Button } from "native-base";
import auth from '@react-native-firebase/auth';

class Welcome extends React.Component {

  componentDidMount(){
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'routeHome' })],
    });

    const { navigate, dispatch } = this.props.navigation;

    auth().onAuthStateChanged(function(user) {
      if (user) {
        console.log('User has Logged')
        navigate('routeHome')
      }
    });  
  }

  render() {
    const{navigate}=this.props.navigation;
    return (
      <LinearGradient
        start={{x: 0, y: 0.25}} end={{x: 1, y: 1.8}}
        locations={[0,0.2,0.7]}
        colors={['#1C87BB','#3FBCBE','#059677']}
        style={{flex:1,padding:25,paddingVertical:50}}>
          <Content contentContainerStyle={{flex:1}}>
            <Col style={styles.col1}>
              <Text style={{fontSize:80,color:'white',fontWeight: 'bold'}}>bide!</Text>
              <Text style={{fontSize:40,color:'white'}}>admin</Text>
            </Col>
            <Col style={{flex:1,justifyContent:'flex-end'}}>
              <Button onPress={()=>navigate('Login')} style={{borderRadius:20,justifyContent:'center',backgroundColor:'white',borderWidth:0,marginBottom:20}}>
                <Text style={{fontSize:18,fontWeight:'bold',color:'#1DA696'}}>Login</Text>
              </Button>
              <Button onPress={()=>navigate('Registration')} transparent style={{borderWidth:2,borderColor:'white',borderRadius:20,backgroundColor:'',justifyContent:'center'}}>
                <Text style={{fontSize:18,fontWeight:'bold',color:'white'}}>Register</Text>
              </Button>
            </Col>
          </Content>
      </LinearGradient>
    );
  }
}

export default Welcome;

const styles = StyleSheet.create({
    col1: {
      alignItems:'center',
    },
    sf5bacfa4: {
      fontSize: 25,
      textAlign: `center`,
      fontWeight: `400`
    },
    sf6f58cc7: {
      flex: 1,
      flexDirection: `column`,
      justifyContent: `center`
    },
    s28bd15d1: {
      fontSize: 25,
      width: `90%`,
      fontWeight: `400`,
      borderBottomWidth: 3,
      borderBottomColor: `rgba(0, 145, 95, 1)`,
      paddingLeft: 10
    },
    s1157eaee: {
      flex: 3,
      justifyContent: `center`
    },
    s4bb7eeaf: {
      flexDirection: `row`,
      justifyContent: `center`,
      alignItems: `stretch`,
      marginBottom: 20
    },
    s44f53c6e: {
      fontSize: 11,
      fontWeight: `400`,
      color: `rgba(0, 145, 95, 1)`,
      marginBottom: 20
    },
    s29adf86c: {
      textAlign: `center`,
      fontSize: 20,
      color: `rgba(255, 255, 255, 1)`,
      fontWeight: `400`
    },
    scf3d97cc: {
      backgroundColor: `rgba(0, 145, 95, 1)`,
      justifyContent: `center`,
      borderRadius: 75,
      alignItems: `stretch`,
      height: 55,
      borderColor: 'white',
      borderWidth: 1
    },
    s59ba8be0: {
      justifyContent: `center`,
      alignItems: `stretch`,
      marginBottom: 2,
      marginTop: 4
    },
    s48aa0650: {
      flex: 4,
      alignItems: `stretch`,
      flexDirection: `column`,
      padding: 20
    },
    s48aa0651: {
        flex: 1,
        alignItems: `stretch`,
        flexDirection: `column`,
        padding: 20
      },
    s13f64db2: {
      flex: 1,
      alignItems: `stretch`,
      flexDirection: `column`,
      backgroundColor: `rgba(255, 255, 255, 1)`
    }
  });